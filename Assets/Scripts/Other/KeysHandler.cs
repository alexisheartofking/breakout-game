using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeysHandler : MonoBehaviour
{
	[System.Serializable]
	private struct Key
	{
		public KeyCode KeyCode;
		public Sprite KeySprite;
		public string KeyName;
	}

	[SerializeField]
	private List<Key> keys;

	public Sprite GetKeySprite(KeyCode KeyCode)
	{
		int length = keys.Count;
		for (int i = 0; i < length; ++i)
		{
			Key key = keys[i];

			if (key.KeyCode == KeyCode)
				return key.KeySprite;
		}

		return null;
	}

	public Sprite GetKeyNameSprite(string KeyName)
	{
		int length = keys.Count;
		for (int i = 0; i < length; ++i)
		{
			Key key = keys[i];

			if (key.KeyName == KeyName)
				return key.KeySprite;
		}

		return null;
	}
}
