using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayHandler : MonoBehaviour
{
	[SerializeField]
	private ParticleSystem congratulationsParticleSystem;
	[SerializeField]
	private GameObject[] levelsPrefabArray;

	private GameplayUIHandler gameplayUIHandler;
	private GroundHandler groundHandler;
	private PlayerController playerController;
	private BallBehaviour ballBehaviour;

	private LevelHandler currentLevel;

	private int LastLevelLoaded = -1;
	private bool IsMatchOver = false;
	private bool hasStarted = false;
	private bool skipFrame = false;

	public void Awake()
	{
		gameplayUIHandler = FindObjectOfType<GameplayUIHandler>();
		if (gameplayUIHandler == null)
			Debug.LogError("Missing GameplayUIHandler in scene");

		groundHandler = FindObjectOfType<GroundHandler>();
		if (groundHandler == null)
			Debug.LogError("Missing GroundHandler in scene");

		playerController = FindObjectOfType<PlayerController>();
		if (playerController == null)
			Debug.LogError("Missing PlayerController in scene");

		ballBehaviour = FindObjectOfType<BallBehaviour>();
		if (ballBehaviour == null)
			Debug.LogError("Missing BallBehaviour in scene");

		if (gameplayUIHandler != null)
			gameplayUIHandler.GetTimerHandler().d_HalfTimeOver += HalfTimeOver;
	}

	public void Initialize(int CurrentLevel)
	{
		// Clear old level
		if (LastLevelLoaded >= 0)
			GameObject.Destroy(currentLevel.gameObject);

		if (CurrentLevel == 0)
			gameplayUIHandler.GetButtonTutorialHandler().ShowButtonTutorial(God.Instance.ControllerType);
		else if (CurrentLevel == 1)
			gameplayUIHandler.GetButtonTutorialHandler().HideTutorial();

		LastLevelLoaded = CurrentLevel;
		currentLevel = GameObject.Instantiate(levelsPrefabArray[CurrentLevel]).GetComponent<LevelHandler>();

		gameplayUIHandler.GetScoreHandler().SetScoreboard(Team.JAPAN, (int)currentLevel.CurrentLevelSettings.StartScore.x, currentLevel.CurrentLevelSettings.EnemyTeam, (int)currentLevel.CurrentLevelSettings.StartScore.y);
		gameplayUIHandler.GetTimerHandler().Reset(currentLevel.CurrentLevelSettings.HalfState);

		groundHandler.SetGroundState(currentLevel.CurrentLevelSettings.GroundState);

		// Initialization for Special Shot
		playerController.Initialize();
		playerController.canUseSpecialShoot = currentLevel.CurrentLevelSettings.canUseSpecialShot;
		gameplayUIHandler.GetSpecialShotUIHandler().SetState(currentLevel.CurrentLevelSettings.canUseSpecialShot);

		gameplayUIHandler.GetLevelExplanationUIHandler().ShowLevelExplanation(CurrentLevel);

		ballBehaviour.Initialize();

		God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.START_LEVEL, 0.2f);

		IsMatchOver = false;
		hasStarted = false;
		playerController.enabled = false;
	}

	public void Update()
	{
		// Pause menu input
		if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.JoystickButton7))
		{
			gameplayUIHandler.GetPauseMenuHandler().Open();
		}
		else if (!hasStarted && Input.GetKeyDown(playerController.GetPlayerKeys().TacleKey))
		{
			if (skipFrame)
			{
				skipFrame = false;
			}
			else
			{
				gameplayUIHandler.GetLevelExplanationUIHandler().HideLevelExplanation();
				playerController.enabled = true;
				hasStarted = true;
			}
		}
	}

	/// <summary>
	/// Skip next update frame (for input doublon)
	/// </summary>
	public void SkipNextFrame()
	{
		skipFrame = true;
	}

	/// <summary>
	/// Call when Player has scored
	/// </summary>
	public void PlayerScored()
	{
		gameplayUIHandler.GetScoreHandler().FirstTeamScored();
		gameplayUIHandler.GetScoringHandler().AddScore(ScoringInfos.ScoringType.PLAYER_GOAL, 1);

		playerController.Reset();
		gameplayUIHandler.GetTimerHandler().PauseTimer();

		playerController.enabled = false;

		God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.GOAL, 0.2f);

		gameplayUIHandler.GetAlertHandler().StartBlinkAlert(gameplayUIHandler.GetScoreHandler().GetTeam1Infos().FullName + " has scored!!!", 3f, gameplayUIHandler.GetScoreHandler().GetTeam1Infos().Color);
	}

	/// <summary>
	/// Call when enemy has scored
	/// </summary>
	public void EnemyScored()
	{
		gameplayUIHandler.GetScoreHandler().SecondTeamScored();
		playerController.Reset();
		gameplayUIHandler.GetTimerHandler().PauseTimer();

		God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.GOAL, 0.2f);

		gameplayUIHandler.GetAlertHandler().StartBlinkAlert(gameplayUIHandler.GetScoreHandler().GetTeam2Infos().FullName + " has scored!!!", 3f, gameplayUIHandler.GetScoreHandler().GetTeam2Infos().Color);

		playerController.enabled = false;
	}

	/// <summary>
	/// Call when ball goes out of bound (usually for eagle shot)
	/// </summary>
	public void BallOutOfBounds()
	{
		playerController.Reset();
		gameplayUIHandler.GetTimerHandler().PauseTimer();
		gameplayUIHandler.GetAlertHandler().StartAlert("BALL\nis out of bounds", 2f, Color.white);

		God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.OUT_OF_BOUNDS, 0.2f);

		playerController.enabled = false;
	}

	/// <summary>
	/// Restart gameplay and player can play again
	/// </summary>
	public void RestartGameplay()
	{
		if (IsMatchOver)
			LevelOver();
		else
			playerController.enabled = true;
	}

	/// <summary>
	/// Call when a halftime is over
	/// </summary>
	/// <param name="HalfState">Type of halftime</param>
	public void HalfTimeOver(TimerHandler.HalfState HalfState)
	{
		if (HalfState == TimerHandler.HalfState.FIRST_HALF)
		{
			playerController.Reset();
			gameplayUIHandler.GetTimerHandler().PauseTimer();
			playerController.enabled = false;

			currentLevel.GetEnemiesHandler().UpdateAllEnemiesStamina(50f);

			gameplayUIHandler.GetAlertHandler().StartAlert("HALF-TIME\nEnemies regain 50% of stamina!", 4f, Color.white);

			God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.HALF_OVER, 0.2f);
		}
		else if (HalfState == TimerHandler.HalfState.SECOND_HALF)
		{
			playerController.enabled = false;
			ballBehaviour.Stop();

			if (GetGameplayUIHandler().GetScoreHandler().HasPlayerWon())
			{
				if (LastLevelLoaded + 1 < levelsPrefabArray.Length)
				{
					gameplayUIHandler.GetAlertHandler().StartAlert("Match IS OVER!\nYou have WON", 4f, new Color(255f / 255f, 191f / 255f, 0f));
					God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.LEVEL_WIN, 0.2f);
					God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.GOAL, 0.2f);
				}
				else
				{
					congratulationsParticleSystem.Stop();
					ParticleSystem.MainModule mainModule = congratulationsParticleSystem.main;
					mainModule.duration = 3f;
					congratulationsParticleSystem.Play();

					gameplayUIHandler.GetAlertHandler().StartCongratulationsAlert("CONGRATULATIONS!\nYou're the new world champion!!!", 5f);
					God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.LEVEL_WIN, 0.2f);
					God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.GOAL, 0.2f);
				}
			}
			else
			{
				gameplayUIHandler.GetAlertHandler().StartAlert("Match IS OVER!\nYou have LOST", 4f, Color.red);
				God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.GAME_OVER_VOICE, 0.2f);
				God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.OUT_OF_BOUNDS, 0.2f);
			}

			IsMatchOver = true;
		}
	}

	/// <summary>
	/// Call when level is over
	/// </summary>
	private void LevelOver()
	{
		gameplayUIHandler.GetScoreHandler().ComputeScoreDifferenceToScore();

		if (LastLevelLoaded + 1 > levelsPrefabArray.Length)
		{
			God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetScoringHandler().AddScore(ScoringInfos.ScoringType.ALL_LEVEL_DONE, 1);
			God.Instance.GameOver(gameplayUIHandler.GetScoringHandler().GetScore());
		}
		else
		{
			if (CanPlayerGoNextLevel())
			{
				God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetScoringHandler().AddScore(ScoringInfos.ScoringType.LEVEL_OVER, 1);
				Initialize(LastLevelLoaded + 1);
			}
			else
				God.Instance.GameOver(gameplayUIHandler.GetScoringHandler().GetScore());
		}

	}

	/// <summary>
	/// Call to go to next level
	/// </summary>
	/// <returns></returns>
	public bool CanPlayerGoNextLevel()
	{
		return gameplayUIHandler.GetScoreHandler().HasPlayerWon();
	}

	// GETTER
	public GameplayUIHandler GetGameplayUIHandler()
	{
		if (gameplayUIHandler == null)
			Debug.LogError("gameplayUIHandler is null");

		return gameplayUIHandler;
	}

	public GroundHandler GetGroundHandler()
	{
		if (groundHandler == null)
			Debug.LogError("groundHandler is null");

		return groundHandler;
	}

	public PlayerController GetPlayerController()
	{
		if (playerController == null)
			Debug.LogError("playerController is null");

		return playerController;
	}

	public BallBehaviour GetBallBehaviour()
	{
		if (ballBehaviour == null)
			Debug.LogError("ballBehaviour is null");

		return ballBehaviour;
	}
}
