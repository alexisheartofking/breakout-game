using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyHandler : MonoBehaviour
{
	[SerializeField]
	private Image keyImage;

	public void SetKey(KeyCode KeyCode)
	{
		keyImage.sprite = God.Instance.GetKeysHandler().GetKeySprite(KeyCode);
	}
}
