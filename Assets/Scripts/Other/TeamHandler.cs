using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Team
{
	JAPAN = 0,
	URUGUAY = 1,
	ITALIA = 2,
	SWEDEN = 3,
	NETHERLANDS = 4,
	BRAZIL = 5,
}

public class TeamHandler : MonoBehaviour
{
	[System.Serializable]
	public struct TeamInfos
	{
		public string FullName;
		public string ShortName;
		public Sprite Skin;
		public Sprite LegSkin;
		public Color Color;
	}

	[SerializeField]
	private TeamInfos[] TeamInfosArray;

	public TeamInfos GetTeamInfos(Team Team)
	{
		return TeamInfosArray[(int)Team];
	}
}
