using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoringInfos : MonoBehaviour
{
	public enum ScoringType
	{
		ENEMY_PER_STAMINA = 0,
		PLAYER_GOAL = 1,
		GOAL_DIFFERENCE = 2,
		LEVEL_OVER = 3,
		ALL_LEVEL_DONE = 4
	}

	[System.Serializable]
	public class HighscoreInfos
	{
		public int Score;
		public int EnemyKnockedOut;
		public int PlayerGoals;
		public int GoalDifference;
		public int LevelDone;

		public bool IsNew;

		public HighscoreInfos()
		{
			Score = -1;
			EnemyKnockedOut = -1;
			PlayerGoals = -1;
			GoalDifference = -1;
			LevelDone = -1;
			IsNew = false;
		}

		public HighscoreInfos(int InScore, int InEnemyKnockedOut, int InPlayerGoals, int InGoalDifference, int InLevelDone)
		{
			Score = InScore;
			EnemyKnockedOut = InEnemyKnockedOut;
			PlayerGoals = InPlayerGoals;
			GoalDifference = InGoalDifference;
			LevelDone = InLevelDone;
			IsNew = false;
		}
	}

	[System.Serializable]
    private struct Infos
	{
		public ScoringType ScoringType;
		public int Score;
		public int Stats;
	}

	[SerializeField]
	private Infos[] scoringInfos;

	/// <summary>
	/// Get scoring info then update stats
	/// </summary>
	/// <param name="Value"></param>
	public int GetScoringInfoAndUpdateStats(ScoringType ScoringType, int Value)
	{
		scoringInfos[(int)ScoringType].Stats += Value;
		return scoringInfos[(int)ScoringType].Score;
	}

	/// <summary>
	/// Compute highscore
	/// Do not call directly use instead God.ComputeHighscore()
	/// </summary>
	/// <param name="TotalScore">Total score</param>
	/// <returns></returns>
	public HighscoreInfos ComputeHighscore(int TotalScore)
	{
		return new HighscoreInfos(
			TotalScore,
			scoringInfos[(int)ScoringType.ENEMY_PER_STAMINA].Stats,
			scoringInfos[(int)ScoringType.PLAYER_GOAL].Stats,
			scoringInfos[(int)ScoringType.GOAL_DIFFERENCE].Stats,
			scoringInfos[(int)ScoringType.LEVEL_OVER].Stats
		);
	}
}
