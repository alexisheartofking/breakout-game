using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum ControllerType
{
	KEYBOARD = 0,
	GAMEPAD = 1
}

[RequireComponent(typeof(TeamHandler))]
[RequireComponent(typeof(KeysHandler))]
[RequireComponent(typeof(ScoringInfos))]
[RequireComponent(typeof(SaveHandler))]
[RequireComponent(typeof(SoundsHandler))]
public class God : MonoBehaviour
{
	#region SINGLETON
	private static God instance;
	public static God Instance
	{
		get
		{
			if (instance == null)
			{
				instance = GameObject.FindObjectOfType<God>();

				if (instance == null)
				{
					GameObject container = new GameObject("God");
					instance = container.AddComponent<God>();
				}
			}

			return instance;
		}
	}
	#endregion

	[Header("Debug")]
	public bool ForceLevel;
	public int ForceLevelIndex;
	public ControllerType ForceControllerType;

	[HideInInspector]
	public ControllerType ControllerType;

	private GameplayHandler gameplayHandler;
	private MainMenuUIHandler mainMenuUIHandler;

	private TeamHandler teamHandler;
	private KeysHandler keysHandler;
	private ScoringInfos scoringInfos;
	private SaveHandler saveHandler;
	private SoundsHandler soundsHandler;

	private int currentLevel;

	private int playerTotalScore = 0;
	private MenuType menuToOpen;

	private void Awake()
	{
		Application.targetFrameRate = 60;

		DontDestroyOnLoad(gameObject);

		teamHandler = GetComponent<TeamHandler>();
		keysHandler = GetComponent<KeysHandler>();
		scoringInfos = GetComponent<ScoringInfos>();
		saveHandler = GetComponent<SaveHandler>();
		soundsHandler = GetComponent<SoundsHandler>();

		SceneManager.sceneLoaded += OnSceneLoaded;

		saveHandler.Load();

		menuToOpen = MenuType.MAIN;

		if (ForceLevel)
		{
			ControllerType = ForceControllerType;
			StartLevel(ForceLevelIndex, ForceControllerType);
		}
		else
		{
			SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
		}
	}

	/// <summary>
	/// To call to start level
	/// </summary>
	/// <param name="Level">Level index in array</param>
	public void StartLevel (int Level, ControllerType InControllerType)
	{
		ControllerType = InControllerType;
		currentLevel = Level;

		// Load gameplay scene
		SceneManager.LoadSceneAsync(2, LoadSceneMode.Single);
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		// MAIN MENU
		if (scene.buildIndex == 1)
		{
			Cursor.lockState = CursorLockMode.Confined;
			Cursor.visible = true;

			mainMenuUIHandler = FindObjectOfType<MainMenuUIHandler>();

			if (menuToOpen == MenuType.HIGHSCORES)
			{
				Hashtable parameters = new Hashtable();
				parameters.Add("SetNewHighscore", playerTotalScore >= 0);
				mainMenuUIHandler.OpenMenu(menuToOpen, parameters);
			}
			else
				mainMenuUIHandler.OpenMenu(menuToOpen);

			soundsHandler.PlaySound(SoundsHandler.SoundType.MAIN_MENU_MUSIC, 0.6f, true);
		}
		// GAMEPLAY SCENE
		else if (scene.buildIndex == 2)
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;

			gameplayHandler = FindObjectOfType<GameplayHandler>();
			gameplayHandler.Initialize(currentLevel);

			soundsHandler.PlaySound(SoundsHandler.SoundType.GAMEPLAY_MUSIC, 0.6f, true);
		}
	}

	/// <summary>
	/// Call when the game is over
	/// </summary>
	/// <param name="Score">Player's total score</param>
	public void GameOver(int Score)
	{
		playerTotalScore = Mathf.Max(0, Score);

		menuToOpen = MenuType.HIGHSCORES;
		SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
	}

	/// <summary>
	/// Go back to main menu level
	/// </summary>
	public void GoBackToMainMenu()
	{
		menuToOpen = MenuType.MAIN;
		SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
	}

	/// <summary>
	/// Go to highscores level
	/// </summary>
	public void GoToHighScores()
	{
		playerTotalScore = -1;

		menuToOpen = MenuType.HIGHSCORES;
		SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
	}

	/// <summary>
	/// Compute highscore with total score and return the highscores ranking updated
	/// </summary>
	/// <returns></returns>
	public HighScoreSaved ComputeHighscore()
	{
		saveHandler.AddHighscore(scoringInfos.ComputeHighscore(playerTotalScore));
		return saveHandler.GetPlayerSave().GetHighscores();
	}

	// GETTER
	public GameplayHandler GetGameplayHandler()
	{
		if (gameplayHandler == null)
			Debug.LogError("gameplayHandler is null");

		return gameplayHandler;
	}

	public MainMenuUIHandler GetMainMenuUIHandler()
	{
		if (mainMenuUIHandler == null)
			Debug.LogError("mainMenuUIHandler is null");

		return mainMenuUIHandler;
	}

	public TeamHandler GetTeamHandler()
	{
		if (teamHandler == null)
			Debug.LogError("teamHandler is null");

		return teamHandler;
	}

	public KeysHandler GetKeysHandler()
	{
		if (keysHandler == null)
			Debug.LogError("keysHandler is null");

		return keysHandler;
	}

	public ScoringInfos GetScoringInfos()
	{
		if (scoringInfos == null)
			Debug.LogError("scoringInfos is null");

		return scoringInfos;
	}

	public SaveHandler GetSaveHandler()
	{
		if (saveHandler == null)
			Debug.LogError("saveHandler is null");

		return saveHandler;
	}

	public PlayerController.PlayerKeys GetPlayerKeys()
	{
		Settings settings = GetSaveHandler().GetPlayerSave().GetSettings();

		return new PlayerController.PlayerKeys(
			ControllerType,
			settings.BindArray[(int)Settings.BindType.LEFT].KeyCodes[(int)ControllerType],
			settings.BindArray[(int)Settings.BindType.RIGHT].KeyCodes[(int)ControllerType],
			settings.BindArray[(int)Settings.BindType.TACKLE].KeyCodes[(int)ControllerType],
			settings.BindArray[(int)Settings.BindType.SPECIAL_1].KeyCodes[(int)ControllerType],
			settings.BindArray[(int)Settings.BindType.SPECIAL_2].KeyCodes[(int)ControllerType],
			settings.BindArray[(int)Settings.BindType.SPECIAL_3].KeyCodes[(int)ControllerType]
		);
	}

	public SoundsHandler GetSoundsHandler()
	{
		if (soundsHandler == null)
			Debug.LogError("soundsHandler is null");

		return soundsHandler;
	}
}
