using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemiesHandler))]
public class LevelHandler : MonoBehaviour
{
	[System.Serializable]
	public struct LevelSettings
	{
		public Team EnemyTeam;
		public GroundState GroundState;
		public Vector2 StartScore;
		public TimerHandler.HalfState HalfState;
		public bool canUseSpecialShot;
	}

	public LevelHandler.LevelSettings CurrentLevelSettings;

	private EnemiesHandler enemiesHandler;

	private void Awake()
	{
		enemiesHandler = GetComponent<EnemiesHandler>();
		enemiesHandler.Initialized(CurrentLevelSettings.EnemyTeam);
	}

	public EnemiesHandler GetEnemiesHandler()
	{
		if (enemiesHandler == null)
			Debug.LogError("enemiesHandler is null");

		return enemiesHandler;
	}
}
