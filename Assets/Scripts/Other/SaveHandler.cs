using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class PlayerSave
{
	[SerializeField]
	private HighScoreSaved highScores;
	[SerializeField]
	private Settings settings;

	public PlayerSave()
	{
		highScores = new HighScoreSaved();
		settings = new Settings();
	}

	public HighScoreSaved GetHighscores()
	{
		return highScores;
	}

	public Settings GetSettings()
	{
		return settings;
	}
}

[System.Serializable]
public class HighScoreSaved
{
	public ScoringInfos.HighscoreInfos[] highscoresInfos;

	public HighScoreSaved()
	{
		highscoresInfos = new ScoringInfos.HighscoreInfos[5];
	}
}

[System.Serializable]
public class Settings
{
	[System.Serializable]
	public enum BindType
	{
		LEFT = 0,
		RIGHT = 1,
		TACKLE = 2,
		SPECIAL_1 = 3,
		SPECIAL_2 = 4,
		SPECIAL_3 = 5
	}

	[System.Serializable]
	public struct Bind
	{
		public KeyCode[] KeyCodes;

		public Bind(KeyCode InKeyboardKeyCode, KeyCode InGamepadKeyCode)
		{
			KeyCodes = new KeyCode[2];

			KeyCodes[(int)ControllerType.KEYBOARD] = InKeyboardKeyCode;
			KeyCodes[(int)ControllerType.GAMEPAD] = InGamepadKeyCode;
		}
	}

	public Bind[] BindArray;
	public float SoundsVolume;

	public Settings()
	{
		BindArray = new Bind[6];
		BindArray[(int)Settings.BindType.LEFT] = new Bind(KeyCode.LeftArrow, KeyCode.None);
		BindArray[(int)Settings.BindType.RIGHT] = new Bind(KeyCode.RightArrow, KeyCode.None);
		BindArray[(int)Settings.BindType.TACKLE] = new Bind(KeyCode.Space, KeyCode.JoystickButton0);
		BindArray[(int)Settings.BindType.SPECIAL_1] = new Bind(KeyCode.W, KeyCode.JoystickButton3);
		BindArray[(int)Settings.BindType.SPECIAL_2] = new Bind(KeyCode.X, KeyCode.JoystickButton2);
		BindArray[(int)Settings.BindType.SPECIAL_3] = new Bind(KeyCode.C, KeyCode.JoystickButton1);

		SoundsVolume = 1f;
	}
}

public class SaveHandler : MonoBehaviour
{
	[SerializeField]
	private PlayerSave playerSave;

	/// <summary>
	/// Load player save or create it if there is none
	/// </summary>
	public void Load()
	{
		if (PlayerPrefs.HasKey("PlayerSave"))
		{
			playerSave = JsonUtility.FromJson<PlayerSave>(PlayerPrefs.GetString("PlayerSave"));
		}
		else
		{
			playerSave = new PlayerSave();
			Save();
		}

		for (int i = 0; i < playerSave.GetHighscores().highscoresInfos.Length; ++i)
			playerSave.GetHighscores().highscoresInfos[i].IsNew = false;

		AudioListener.volume = playerSave.GetSettings().SoundsVolume;
	}

	/// <summary>
	/// Save player save
	/// </summary>
	public void Save()
	{
		playerSave.GetSettings().SoundsVolume = AudioListener.volume;
		PlayerPrefs.SetString("PlayerSave", JsonUtility.ToJson(playerSave));
	}

	/// <summary>
	/// Set Bind Key
	/// </summary>
	/// <param name="BindType">Action to rebind</param>
	/// <param name="KeyboardKeyCode">Keyboard key</param>
	/// <param name="GamepadKeyCode">Gamepad key</param>
	public void SetKey(Settings.BindType BindType, KeyCode KeyboardKeyCode, KeyCode GamepadKeyCode)
	{
		playerSave.GetSettings().BindArray[(int)BindType] = new Settings.Bind(KeyboardKeyCode, GamepadKeyCode);
	}

	/// <summary>
	/// Set bind controller key
	/// </summary>
	/// <param name="BindType">Action to rebind</param>
	/// <param name="ControllerType">Controller key to rebind</param>
	/// <param name="KeyCode">Key to bind</param>
	public void SetKey(Settings.BindType BindType, ControllerType ControllerType, KeyCode KeyCode)
	{
		playerSave.GetSettings().BindArray[(int)BindType].KeyCodes[(int)ControllerType] = KeyCode;
	}

	/// <summary>
	/// Set bind keyboard Key
	/// </summary>
	/// <param name="BindType">Action to rebind</param>
	/// <param name="KeyboardKeyCode">Keyboard key</param>
	public void SetKeyboardKey(Settings.BindType BindType, KeyCode KeyboardKeyCode)
	{
		playerSave.GetSettings().BindArray[(int)BindType].KeyCodes[(int)ControllerType.KEYBOARD] = KeyboardKeyCode;
		Save();
	}

	/// <summary>
	/// Set bind gamepad Key
	/// </summary>
	/// <param name="BindType">Action to rebind</param>
	/// <param name="KeyboardKeyCode">Gamepad key</param>
	public void SetGamepadKey(Settings.BindType BindType, KeyCode GamepadKeyCode)
	{
		playerSave.GetSettings().BindArray[(int)BindType].KeyCodes[(int)ControllerType.GAMEPAD] = GamepadKeyCode;
		Save();
	}

	/// <summary>
	/// Add new highscore if in ranking
	/// </summary>
	/// <param name="NewHighscore">Highscore to add</param>
	public void AddHighscore(ScoringInfos.HighscoreInfos NewHighscore)
	{
		ScoringInfos.HighscoreInfos previousHighscoreSaved = null;

		for (int i = 0; i < playerSave.GetHighscores().highscoresInfos.Length; ++i)
		{
			ScoringInfos.HighscoreInfos highscoreSaved = playerSave.GetHighscores().highscoresInfos[i];
			highscoreSaved.IsNew = false;

			if (previousHighscoreSaved != null)
			{
				playerSave.GetHighscores().highscoresInfos[i] = previousHighscoreSaved;
				previousHighscoreSaved = highscoreSaved;
			}
			else if (highscoreSaved.Score <= NewHighscore.Score)
			{
				previousHighscoreSaved = highscoreSaved;
				NewHighscore.IsNew = true;
				playerSave.GetHighscores().highscoresInfos[i] = NewHighscore;
			}
		}

		Save();
	}

	/// <summary>
	/// Set sounds volume
	/// </summary>
	/// <param name="Volume">Volume to set</param>
	public void SetSoundsVolume(float Volume)
	{
		playerSave.GetSettings().SoundsVolume = Volume;
	}

	public PlayerSave GetPlayerSave()
	{
		return playerSave;
	}
}
