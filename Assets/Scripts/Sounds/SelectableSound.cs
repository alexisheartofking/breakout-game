using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectableSound : MonoBehaviour, ISelectHandler
{
	public void OnSelect(BaseEventData eventData)
	{
		God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.UI_SELECT, 0.2f);
	}

	public void OnClick()
	{
		God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.UI_VALIDATION, 0.2f);
	}
}
