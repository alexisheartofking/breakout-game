using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundsHandler : MonoBehaviour
{
	public enum SoundType
	{
		BALL_IMPACT = 0,
		BALL_SPECIAL_SHOT = 1,
		ENEMY_OUT_OF_STAMINA = 2,
		GAME_OVER_VOICE = 3,
		SPECIAL_READY = 4,
		HALF_OVER = 5,
		LAST_SECONDS = 6,
		LEVEL_WIN = 7,
		MAIN_MENU_MUSIC = 8,
		NEW_HIGHSCORE = 9,
		OUT_OF_BOUNDS = 10,
		GAMEPLAY_MUSIC = 11,
		UI_ERROR = 12,
		UI_SELECT = 13,
		UI_VALIDATION = 14,
		GOAL = 15,
		START_LEVEL = 16
	}

	[SerializeField]
	private GameObject soundPrefab;
	[SerializeField]
	private AudioClip[] sounds;

	public void PlaySound(SoundType SoundType, float Volume = 1f, bool IsLoop = false)
	{
		SoundHandler soundHandler = GameObject.Instantiate(soundPrefab).GetComponent<SoundHandler>();
		soundHandler.PlaySound(sounds[(int)SoundType], Volume, IsLoop);
	}
}
