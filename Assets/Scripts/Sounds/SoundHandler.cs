using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundHandler : MonoBehaviour
{
	private AudioSource audioSource;

	private float timeSound = -1f;
	private float timerSound = 0f;

	private void Awake()
	{
		audioSource = GetComponent<AudioSource>();
	}

	private void Update()
	{
		if (timeSound > 0f)
		{
			timerSound += Time.deltaTime;

			if (timerSound >= timeSound)
				Destroy(gameObject);
		}
	}

	/// <summary>
	/// Play sound
	/// </summary>
	/// <param name="AudioClip">Sound to play</param>
	/// <param name="Volume">Volume to play sound</param>
	/// <param name="IsLoop">Is sound a loop</param>
	public void PlaySound(AudioClip AudioClip, float Volume, bool IsLoop)
	{
		audioSource.clip = AudioClip;
		audioSource.volume = Volume;
		audioSource.loop = IsLoop;

		if (IsLoop == false)
			timeSound = AudioClip.length;

		audioSource.Play();
	}
}
