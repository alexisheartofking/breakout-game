using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonSound : SelectableSound
{
	private Button button;

	private void Awake()
	{
		button = GetComponent<Button>();

		button.onClick.AddListener(OnClick);
	}
}
