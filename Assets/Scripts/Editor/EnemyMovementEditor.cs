using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine.UI;

[CustomEditor(typeof(EnemyMovement))]
[CanEditMultipleObjects]
public class EnemyMovementEditor : Editor
{
	private ReorderableList positionsEditor;
	private int indexSelectedElement;
	private EnemyMovement myTarget;
	private GUIStyle numberStyle;

	void OnEnable()
	{
		myTarget = (EnemyMovement)target;

		GUI.contentColor = Color.red;
		numberStyle = new GUIStyle();
		numberStyle.fontSize = 25;

		positionsEditor = new ReorderableList(serializedObject, serializedObject.FindProperty("CurrentMovementInfos").FindPropertyRelative("Positions"), true, true, true, true);

		positionsEditor.elementHeightCallback =
			(int index) =>
			{
				float propertyHeight = EditorGUI.GetPropertyHeight(positionsEditor.serializedProperty.GetArrayElementAtIndex(index), true);
				float spacing = EditorGUIUtility.singleLineHeight / 2;

				return propertyHeight + spacing;
			};

		positionsEditor.drawHeaderCallback = 
			(Rect rect) => 
			{
				EditorGUI.LabelField(rect, "Points List");
			};

		positionsEditor.drawElementCallback =
			(Rect rect, int index, bool isActive, bool isFocused) =>
			{
				var element = positionsEditor.serializedProperty.GetArrayElementAtIndex(index);
				rect.y += 2;

				EditorGUI.PropertyField(
					new Rect(rect.x + 10, rect.y, 500, EditorGUIUtility.singleLineHeight),
					element,
					true
				);
			};

		positionsEditor.onSelectCallback =
			(ReorderableList l) =>
			{
				indexSelectedElement = l.index;
			};
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		EditorGUILayout.PropertyField(serializedObject.FindProperty("CurrentMovementInfos").FindPropertyRelative("CurrentMovementType"));

		positionsEditor.DoLayoutList();

		if (GUILayout.Button("Apply current object position to selected element"))
			myTarget.SetPositionToCurrent(indexSelectedElement);

		serializedObject.ApplyModifiedProperties();
	}

	public void OnSceneGUI()
	{
		if (myTarget.CurrentMovementInfos.Positions.Length < 1)
			return;

		Handles.color = Color.blue;
		int length = myTarget.CurrentMovementInfos.Positions.Length;
		for (int i = 0; i < length; ++i)
		{
			if (i < length - 1)
				Handles.DrawLine(myTarget.CurrentMovementInfos.Positions[i].Position, myTarget.CurrentMovementInfos.Positions[i + 1].Position);
			else if (length > 2 && myTarget.CurrentMovementInfos.CurrentMovementType == EnemyMovement.MovementType.LOOP)
				Handles.DrawLine(myTarget.CurrentMovementInfos.Positions[i].Position, myTarget.CurrentMovementInfos.Positions[0].Position);

			Handles.DrawWireDisc(myTarget.CurrentMovementInfos.Positions[i].Position, Vector3.forward, 0.01f, 10f);
			Handles.Label(myTarget.CurrentMovementInfos.Positions[i].Position + new Vector3(-0.13f, 0.6f, 0f), i.ToString(), numberStyle);
		}
	}
}
