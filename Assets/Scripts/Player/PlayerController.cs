using UnityEngine;

[RequireComponent(typeof(CapsuleCollider2D))]
public class PlayerController : MonoBehaviour
{
	[System.Serializable]
	public struct PlayerKeys
	{
		public ControllerType ControllerType;

		public KeyCode LeftKey;
		public KeyCode RightKey;
		public KeyCode TacleKey;
		public KeyCode Special1Key;
		public KeyCode Special2Key;
		public KeyCode Special3Key;

		public PlayerKeys(ControllerType InControllerType)
		{
			this.ControllerType = InControllerType;

			if (ControllerType == ControllerType.GAMEPAD)
			{
				this.LeftKey = KeyCode.None;
				this.RightKey = KeyCode.None;
				this.TacleKey = KeyCode.JoystickButton0;
				this.Special1Key = KeyCode.JoystickButton3;
				this.Special2Key = KeyCode.JoystickButton2;
				this.Special3Key = KeyCode.JoystickButton1;

				return;
			}

			this.LeftKey = KeyCode.LeftArrow;
			this.RightKey = KeyCode.RightArrow;
			this.TacleKey = KeyCode.Space;
			this.Special1Key = KeyCode.W;
			this.Special2Key = KeyCode.X;
			this.Special3Key = KeyCode.C;
		}

		public PlayerKeys(ControllerType InControllerType, KeyCode InLeftKey, KeyCode InRightKey, KeyCode InTacleKey, KeyCode InSpecial1Key, KeyCode InSpecial2Key, KeyCode InSpecial3Key)
		{
			this.ControllerType = InControllerType;
			
			this.LeftKey = InLeftKey;
			this.RightKey = InRightKey;
			this.TacleKey = InTacleKey;
			this.Special1Key = InSpecial1Key;
			this.Special2Key = InSpecial2Key;
			this.Special3Key = InSpecial3Key;
		}
	}

	// KEYS
	private PlayerKeys playerKeys;

	[Header("Player State")]
	[SerializeField]
	private PlayerState playerState;

	[Header("Movement")]
	[SerializeField]
	private Transform playerSprite;
	[SerializeField]
	private float maxMovement = 6.6f;
	[SerializeField]
	private float baseSpeed = 2f;

	[HideInInspector]
	public Vector3 direction = Vector3.zero;

	private float currentSpeed;

	[Header("Tacle")]
	[SerializeField]
	private GameObject leftLeg;
	[SerializeField]
	private GameObject rightLegs;
	[SerializeField]
	private float tacleSpeedPercent = 0.25f;

	//Special Shoot
	[HideInInspector]
	public bool canUseSpecialShoot = false;

	[SerializeField]
	private SpriteRenderer specialShotRangeSprite;

	private CapsuleCollider2D coll2D;
	private BallBehaviour ballBehaviour;

	// Position used for reset player
	private Vector3 basePosition;

	public void Awake()
	{
		basePosition = transform.position;
		coll2D = GetComponent<CapsuleCollider2D>();
	}

	public void Initialize()
	{
		// TODO:
		// Get user preference (keyboad, gamepad) for playing and load key from settings
		// Default is keyboard
		playerKeys = God.Instance.GetPlayerKeys();

		ballBehaviour = God.Instance.GetGameplayHandler().GetBallBehaviour();
		playerState.CurrentState = PlayerState.State.HAS_BALL;
		God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetSpecialShotUIHandler().SetSpecialShotsKeys(playerKeys.Special1Key, playerKeys.Special2Key, playerKeys.Special3Key);

		Reset();
	}

	private void Update()
    {
		// Tacle
		if (direction.x > 0f && playerState.CurrentState == PlayerState.State.MOVING)
		{
			if (playerState.CurrentState == PlayerState.State.MOVING)
				playerSprite.rotation = Quaternion.Euler(0f, 0f, 0f);

			if (Input.GetKey(playerKeys.TacleKey))
			{
				currentSpeed = baseSpeed * tacleSpeedPercent;

				leftLeg.SetActive(true);
				rightLegs.SetActive(false);

				coll2D.enabled = false;
			}
			else
			{
				currentSpeed = baseSpeed;

				leftLeg.SetActive(false);
				rightLegs.SetActive(false);

				coll2D.enabled = true;
			}
		}
		else if (direction.x < 0f && playerState.CurrentState == PlayerState.State.MOVING)
		{
			if (playerState.CurrentState == PlayerState.State.MOVING)
				playerSprite.rotation = Quaternion.Euler(0f, 0f, 180f);

			if (Input.GetKey(playerKeys.TacleKey))
			{
				currentSpeed = baseSpeed * tacleSpeedPercent;

				leftLeg.SetActive(false);
				rightLegs.SetActive(true);

				coll2D.enabled = false;
			}
			else
			{
				currentSpeed = baseSpeed;

				leftLeg.SetActive(false);
				rightLegs.SetActive(false);

				coll2D.enabled = true;
			}
		}
		else
		{
			currentSpeed = baseSpeed;

			leftLeg.SetActive(false);
			rightLegs.SetActive(false);

			coll2D.enabled = true;

			playerSprite.rotation = Quaternion.Euler(0f, 0f, 90f);
		}

		// Shoot ball when player has ball
		if (playerState.CurrentState == PlayerState.State.HAS_BALL)
		{
			if (Input.GetKeyDown(playerKeys.TacleKey))
			{
				God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetTimerHandler().StartTimer();
				ballBehaviour.Shoot(BallBehaviour.State.NORMAL);
				playerState.CurrentState = PlayerState.State.MOVING;
			}
		}

		// Movement
		if (playerKeys.ControllerType == ControllerType.KEYBOARD)
		{
			direction = Vector3.zero;

			// Direction
			if (Input.GetKey(playerKeys.LeftKey))
				direction = new Vector3(-1f, 0f, 0f) * currentSpeed * Time.deltaTime;
			else if (Input.GetKey(playerKeys.RightKey))
				direction = new Vector3(1f, 0f, 0f) * currentSpeed * Time.deltaTime;

			MovePlayer();
		}
		else if (playerKeys.ControllerType == ControllerType.GAMEPAD)
		{
			direction = Vector3.zero;
			float movementValue = Input.GetAxis("Horizontal");

			if (movementValue < -0.3f)
				direction = new Vector3(-1f, 0f, 0f) * currentSpeed * Time.deltaTime;
			else if (movementValue > 0.3f)
				direction = new Vector3(1f, 0f, 0f) * currentSpeed * Time.deltaTime;

			MovePlayer();
		}

		if (canUseSpecialShoot && ballBehaviour.CurrentState == BallBehaviour.State.SPECIAL)
		{
			// Special 1 (Eagle Shot)
			if (Input.GetKey(playerKeys.Special1Key))
			{
				specialShotRangeSprite.gameObject.SetActive(true);
				specialShotRangeSprite.color = ballBehaviour.EagleShotColor;

				if (Vector3.Distance(ballBehaviour.transform.position, transform.position) < 1.129f)
					ballBehaviour.SetState(BallBehaviour.State.EAGLE);
			}
			// Special 2 (Curve Shot)
			else if (Input.GetKey(playerKeys.Special2Key))
			{
				specialShotRangeSprite.gameObject.SetActive(true);
				specialShotRangeSprite.color = ballBehaviour.CurveShotColor;

				if (Vector3.Distance(ballBehaviour.transform.position, transform.position) < 1.129f)
					ballBehaviour.SetState(BallBehaviour.State.CURVE);
			}
			// Special 3 (Triple Shot)
			else if (Input.GetKeyDown(playerKeys.Special3Key))
			{
				specialShotRangeSprite.gameObject.SetActive(true);
				specialShotRangeSprite.color = ballBehaviour.tripleShotColor;

				if (Vector3.Distance(ballBehaviour.transform.position, transform.position) < 1.129f)
					ballBehaviour.SetState(BallBehaviour.State.TRIPLE);
			}
			else
			{
				specialShotRangeSprite.gameObject.SetActive(false);
			}
		}
		else
		{
			specialShotRangeSprite.gameObject.SetActive(false);
		}
	}

	/// <summary>
	/// Move the player with direction value
	/// </summary>
	private void MovePlayer()
	{
		this.transform.Translate(direction);

		// Max Movement
		if (this.transform.position.x + direction.x > maxMovement || this.transform.position.x + direction.x < -maxMovement)
		{
			Vector3 position = transform.position;
			position.x = Mathf.Min(maxMovement, Mathf.Max(-maxMovement, position.x));
			transform.position = position;
		}
	}

	public void Reset()
	{
		this.transform.position = basePosition;

		playerState.CurrentState = PlayerState.State.HAS_BALL;
		playerSprite.rotation = Quaternion.Euler(0f, 0f, 90f);

		leftLeg.SetActive(false);
		rightLegs.SetActive(false);

		coll2D.enabled = true;

		ballBehaviour.Reset();
	}

	public PlayerKeys GetPlayerKeys()
	{
		return playerKeys;
	}
}
