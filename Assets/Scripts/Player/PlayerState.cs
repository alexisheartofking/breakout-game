using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour
{
    public enum State
	{
		HAS_BALL,
		MOVING,
		USE_SPECIAL
	}

	[HideInInspector]
	public State CurrentState = State.HAS_BALL;
}
