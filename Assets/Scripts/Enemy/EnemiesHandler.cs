using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesHandler : MonoBehaviour
{
	private EnemyBehaviour[] enemiesArray;

	public void Initialized(Team EnemyTeam)
	{
		enemiesArray = GetComponentsInChildren<EnemyBehaviour>();

		for (int i = 0; i < enemiesArray.Length; ++i)
			enemiesArray[i].SetTeamSkin(EnemyTeam);
	}

	/// <summary>
	/// Update all enemies a fixed value of stamina
	/// </summary>
	/// <param name="Value">Fixed value of stamina to gain</param>
	public void UpdateAllEnemiesStamina(int Value)
	{
		for (int i = 0; i < enemiesArray.Length; ++i)
			enemiesArray[i].Hit(Value);
	}

	/// <summary>
	/// Update all enemies a percent value of stamina
	/// </summary>
	/// <param name="Value">Percent value of stamina to gain</param>
	public void UpdateAllEnemiesStamina(float Value)
	{
		for (int i = 0; i < enemiesArray.Length; ++i)
			enemiesArray[i].Hit(Value);
	}
}
