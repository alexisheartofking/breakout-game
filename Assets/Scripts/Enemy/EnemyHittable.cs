using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHittable : MonoBehaviour
{
	[SerializeField]
	protected StaminaHandler staminaHandler;

	virtual public bool Hit(int Value)
	{
		return staminaHandler.UpdateStamina(Value);
	}

	virtual public bool Hit(float Value)
	{
		return staminaHandler.UpdateStamina(Value);
	}
}
