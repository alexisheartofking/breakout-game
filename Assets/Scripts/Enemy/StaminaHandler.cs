using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaHandler : MonoBehaviour
{
	[Header("Stamina")]
	[SerializeField]
	private Slider staminaSlider;
	[SerializeField]
	private float baseStamina;

	private float currentStamina;

	[Header("Stamina Update Animation")]
	[SerializeField]
	private float staminaUpdateAnimationTime;
	[SerializeField]
	private AnimationCurve staminaUpdateAnimationCurve;

	private float currentstaminaUpdateAnimationTime;
	private float startStamina;
	private bool isUpdatingStamina;

	// Delegate
	public delegate void StaminaReachZeroAnimation();
	public StaminaReachZeroAnimation d_StaminaReachZeroAnimation;

	public delegate void StaminaReachZero();
	public StaminaReachZero d_StaminaReachZero;

	private void Awake()
	{
		currentStamina = baseStamina;
		staminaSlider.transform.gameObject.SetActive(false);
	}

	private void Update()
	{
		// Update animation refilling
		if (isUpdatingStamina)
		{
			currentstaminaUpdateAnimationTime += Time.deltaTime;

			if (currentstaminaUpdateAnimationTime >= staminaUpdateAnimationTime)
			{
				isUpdatingStamina = false;
				staminaSlider.value = currentStamina / baseStamina;
				staminaSlider.transform.gameObject.SetActive(false);

				// Call delegate if stamina has reached 0
				if (currentStamina == 0 && d_StaminaReachZeroAnimation != null)
					d_StaminaReachZeroAnimation();
			}
			else
			{
				float percentStaminaAnimation = staminaUpdateAnimationCurve.Evaluate(currentstaminaUpdateAnimationTime / staminaUpdateAnimationTime);
				staminaSlider.value = (startStamina + ((currentStamina - startStamina) * percentStaminaAnimation)) / baseStamina;
			}
		}
	}

	/// <summary>
	/// Update stamina
	/// </summary>
	/// <param name="Percent">Percent to change stamina (from -100% to 100%)</param>
	/// <returns>Return is out of stamina</returns>
	public bool UpdateStamina(float Percent)
	{
		startStamina = currentStamina;
		currentStamina = Mathf.FloorToInt(Mathf.Max(0, Mathf.Min(baseStamina, currentStamina + baseStamina * (Percent / 100f))));

		ResetAndStartStaminaUpdateAnimation();

		if (currentStamina <= 0f && d_StaminaReachZero != null)
		{
			d_StaminaReachZero();
			return true;
		}

		return false;
	}

	/// <summary>
	/// Refill stamina
	/// </summary>
	/// <param name="value">Fixed value to change stamina (can be positive of negative)</param>
	/// <returns>Return is out of stamina</returns>
	public bool UpdateStamina(int value)
	{
		startStamina = currentStamina;
		currentStamina = Mathf.Max(0, Mathf.Min(baseStamina, currentStamina + value));

		ResetAndStartStaminaUpdateAnimation();

		if (currentStamina <= 0f && d_StaminaReachZero != null)
		{
			d_StaminaReachZero();
			return true;
		}

		return false;
	}

	/// <summary>
	/// Reset and start stamina refill animation
	/// </summary>
	private void ResetAndStartStaminaUpdateAnimation()
	{
		isUpdatingStamina = true;
		currentstaminaUpdateAnimationTime = 0f;
		staminaSlider.transform.gameObject.SetActive(true);
	}

	public float GetStamina()
	{
		return currentStamina;
	}

	public float GetBaseStamina()
	{
		return baseStamina;
	}
}
