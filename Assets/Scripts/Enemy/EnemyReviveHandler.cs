using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyReviveHandler : MonoBehaviour
{
	[SerializeField]
	private Image reviveCooldownImage;
	[SerializeField]
	private Text reviveCooldownText;

	private float reviveAnimationTime;
	private float reviveAnimationTimer;
	private bool updateTimer;

	//Delegate
	public delegate void EnemyReviveCooldownOver();
	public EnemyReviveCooldownOver d_EnemyReviveCooldownOver;

	/// <summary>
	/// Start revive cooldown animation
	/// </summary>
	/// <param name="animationTime">Animation time (time before revive)</param>
	public void StartAnimation(float animationTime)
	{
		reviveAnimationTime = animationTime;
		reviveAnimationTimer = 0f;
		updateTimer = true;

		reviveCooldownImage.gameObject.SetActive(true);
	}

	private void Update()
	{
		if (!updateTimer)
			return;

		reviveAnimationTimer += Time.deltaTime;

		if (reviveAnimationTimer >= reviveAnimationTime)
		{
			updateTimer = false;
			reviveCooldownImage.gameObject.SetActive(false);

			if (d_EnemyReviveCooldownOver != null)
				d_EnemyReviveCooldownOver();
		}
		else
			UpdateUI();
	}

	/// <summary>
	/// Update revive cooldown UI
	/// </summary>
	private void UpdateUI()
	{
		reviveCooldownText.text = ((int)(reviveAnimationTime - reviveAnimationTimer) + 1).ToString();
		reviveCooldownImage.fillAmount = reviveAnimationTime / reviveAnimationTimer;
	}
}
