using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
	[System.Serializable]
	public enum MovementType
	{
		PING_PONG,
		LOOP
	}

	[System.Serializable]
	public struct Point
	{
		public Vector3 Position;
		public float MovementSpeed;
	}

	[System.Serializable]
	public struct MovementInfos
	{
		public MovementType CurrentMovementType;
		public Point[] Positions;
	}

	public MovementInfos CurrentMovementInfos;

	private float CurrentMovementSpeed = 10f;

	private Point startPoint;
	private Point endPoint;
	private int currentIndex = 1;
	private int directionIndex = 1;

	private float percent = 0f;
	private Vector3 direction = Vector3.zero;

	// Editor function
	public void SetPositionToCurrent(int index)
	{
		CurrentMovementInfos.Positions[index].Position = transform.position;
	}

	// Script function
	public void Awake()
	{
		if (CurrentMovementInfos.Positions.Length == 0)
		{
			enabled = false;
			return;
		}
		else if (CurrentMovementInfos.Positions.Length == 1)
		{
			enabled = false;
		}
		else
		{
			startPoint = CurrentMovementInfos.Positions[0];
			endPoint = CurrentMovementInfos.Positions[1];
			currentIndex = 1;

			CurrentMovementSpeed = endPoint.MovementSpeed;
		}

		transform.position = CurrentMovementInfos.Positions[0].Position;
	}

	public void Update()
	{
		direction = (endPoint.Position - transform.position).normalized * CurrentMovementSpeed * Time.deltaTime;

		if (Vector3.Distance(transform.position + direction, endPoint.Position) < 0.05f)
		{
			transform.position = endPoint.Position;
			startPoint = endPoint;

			currentIndex += directionIndex;

			if (currentIndex < CurrentMovementInfos.Positions.Length && currentIndex >= 0)
				endPoint = CurrentMovementInfos.Positions[currentIndex];
			else
			{
				if (CurrentMovementInfos.CurrentMovementType == MovementType.LOOP)
				{
					endPoint = CurrentMovementInfos.Positions[0];
					currentIndex = 0;
				}
				else if (CurrentMovementInfos.CurrentMovementType == MovementType.PING_PONG)
				{
					directionIndex = -directionIndex;
					currentIndex += 2 * directionIndex;

					endPoint = CurrentMovementInfos.Positions[currentIndex];
				}
			}
		}
		else
			transform.Translate(direction);
	}
}
