using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider2D))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(EnemyMovement))]
[RequireComponent(typeof(EnemyReviveHandler))]
public class EnemyBehaviour : EnemyHittable
{
	[SerializeField]
	private SpriteRenderer sprite;
	[SerializeField]
	private SpriteRenderer leftLegs;
	[SerializeField]
	private SpriteRenderer rightLegs;

	[Header("Tacle")]
	[SerializeField]
	private bool canTacle = false;
	[SerializeField]
	private float cooldownTacle = 1f;
	[SerializeField]
	private float tacleTime = 0.3f;

	private bool isTacleOnCD = false;
	private float currentCooldownTacle = 0f;

	private EnemyMovement enemyMovement;
	private EnemyReviveHandler enemyReviveHandler;
	private CapsuleCollider2D coll2D;
	private BoxCollider2D trigger2D;

	private bool isOutOfStamina;
	private float currentTimeBeforeRevive;

	// Delegate
	public delegate void EnemyOutOfStamina();
	public EnemyOutOfStamina d_EnemyOutOfStamina;

	public delegate void EnemyRevive();
	public EnemyRevive d_EnemyRevive;

	private void Awake()
	{
		enemyMovement = GetComponent<EnemyMovement>();
		enemyReviveHandler = GetComponent<EnemyReviveHandler>();

		coll2D = GetComponent<CapsuleCollider2D>();
		trigger2D = GetComponent<BoxCollider2D>();

		staminaHandler.d_StaminaReachZeroAnimation += OnStaminaReachZeroAnimation;
		staminaHandler.d_StaminaReachZero += OnStaminaReachZero;
		enemyReviveHandler.d_EnemyReviveCooldownOver += OnEnemyReviveCooldownOver;

		if (canTacle == false)
			trigger2D.enabled = false;
	}

	private void Update()
	{
		if (isTacleOnCD)
		{
			currentCooldownTacle += Time.deltaTime;

			if (currentCooldownTacle >= cooldownTacle)
				isTacleOnCD = false;
		}
	}

	private void OnTriggerEnter2D(Collider2D Collider)
	{
		if (canTacle && !isTacleOnCD && Collider.CompareTag("Ball"))
		{
			if ((transform.position - Collider.transform.position).x >= 0)
			{
				StartCoroutine(Tacle(leftLegs.gameObject, -180f));
			}
			else if ((transform.position - Collider.transform.position).x < 0)
			{
				StartCoroutine(Tacle(rightLegs.gameObject, 0f));
			}
		}
	}

	private IEnumerator Tacle(GameObject Leg, float ZRotation)
	{
		canTacle = false;

		bool resetMovement = enemyMovement.enabled;
		enemyMovement.enabled = false;

		Leg.SetActive(true);
		sprite.transform.rotation = Quaternion.Euler(0f, 0f, ZRotation);
		coll2D.enabled = false;

		yield return new WaitForSeconds(tacleTime);

		Leg.SetActive(false);
		sprite.transform.rotation = Quaternion.Euler(0f, 0f, -90f);

		enemyMovement.enabled = resetMovement;

		isTacleOnCD = true;
		canTacle = true;

		if (staminaHandler.GetStamina() > 0f)
			coll2D.enabled = true;

		currentCooldownTacle = 0f;
	}

	/// <summary>
	/// Set team sprite of enemy
	/// </summary>
	/// <param name="Team">Team to set as a skin</param>
	public void SetTeamSkin(Team Team)
	{
		TeamHandler.TeamInfos teamInfos = God.Instance.GetTeamHandler().GetTeamInfos(Team);

		sprite.GetComponent<SpriteRenderer>().sprite = teamInfos.Skin;
		leftLegs.GetComponent<SpriteRenderer>().sprite = God.Instance.GetTeamHandler().GetTeamInfos(Team).LegSkin;
		rightLegs.GetComponent<SpriteRenderer>().sprite = God.Instance.GetTeamHandler().GetTeamInfos(Team).LegSkin;
	}

	//Callback

	/// <summary>
	/// Call when enemy died (after stamina animation)
	/// </summary>
	private void OnStaminaReachZeroAnimation()
	{
		God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetScoringHandler().AddScore(ScoringInfos.ScoringType.ENEMY_PER_STAMINA, (int)staminaHandler.GetBaseStamina());
		enemyReviveHandler.StartAnimation(staminaHandler.GetBaseStamina());

		God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.ENEMY_OUT_OF_STAMINA, 0.2f);

		if (d_EnemyOutOfStamina != null)
			d_EnemyOutOfStamina();
	}

	/// <summary>
	/// Call when enemy died (before stamina animation)
	/// </summary>
	private void OnStaminaReachZero()
	{
		coll2D.enabled = false;
		trigger2D.enabled = false;

		ChangeLegsCollidersState(false);

		sprite.color = new Color(1f, 1f, 1f, 0.5f);
	}

	/// <summary>
	/// Call when revive cooldown is over
	/// </summary>
	private void OnEnemyReviveCooldownOver()
	{
		staminaHandler.UpdateStamina(1);

		coll2D.enabled = true;
		trigger2D.enabled = true;

		ChangeLegsCollidersState(true);

		sprite.color = Color.white;

		if (d_EnemyRevive != null)
			d_EnemyRevive();
	}

	public bool IsOutOfStamina()
	{
		return isOutOfStamina;
	}

	private void ChangeLegsCollidersState(bool Active)
	{
		Collider2D[] leftColliders = leftLegs.GetComponents<Collider2D>();
		Collider2D[] rightColliders = rightLegs.GetComponents<Collider2D>();

		for (int i = 0; i < 2; ++i)
		{
			leftColliders[i].enabled = Active;
			rightColliders[i].enabled = Active;
		}
	}
}
