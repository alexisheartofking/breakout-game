using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MenuType
{
	MAIN = 0,
	HIGHSCORES = 1,
	SETTINGS = 2
}

public class MainMenuUIHandler : MonoBehaviour
{
	[SerializeField]
	private Menu[] Menus;

	private Menu currentMenu = null;

	public void OpenMenu(MenuType MenuType, Hashtable Parameters = null)
	{
		if (currentMenu != null)
			currentMenu.Close();

		currentMenu = Menus[(int)MenuType];
		currentMenu.Open(Parameters);
	}

	public void BackMenu()
	{
		currentMenu.Back();
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button1))
		{
			BackMenu();
		}
	}
}
