using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BindHandler : MonoBehaviour
{
	[System.Serializable]
	public struct BindUI
	{
		public Image Image;
		public Button Button;
	}

	[SerializeField]
	private Settings.BindType bindType;

	[SerializeField]
	private BindUI[] bindUIs;

	private void Awake ()
	{
		Settings.Bind bind = God.Instance.GetSaveHandler().GetPlayerSave().GetSettings().BindArray[(int)bindType];

		bindUIs[(int)ControllerType.KEYBOARD].Image.sprite = God.Instance.GetKeysHandler().GetKeySprite(bind.KeyCodes[(int)ControllerType.KEYBOARD]);

		if (bind.KeyCodes[(int)ControllerType.GAMEPAD] == KeyCode.None)
			bindUIs[(int)ControllerType.GAMEPAD].Image.sprite = God.Instance.GetKeysHandler().GetKeyNameSprite("Left Joystick");
		else
			bindUIs[(int)ControllerType.GAMEPAD].Image.sprite = God.Instance.GetKeysHandler().GetKeySprite(bind.KeyCodes[(int)ControllerType.GAMEPAD]);
	}

	public void Select(ControllerType ControllerType)
	{
		bindUIs[(int)ControllerType.KEYBOARD].Button.Select();
	}

	public void UpdateKey(ControllerType ControllerType)
	{
		Settings.Bind bind = God.Instance.GetSaveHandler().GetPlayerSave().GetSettings().BindArray[(int)bindType];

		if (ControllerType == ControllerType.KEYBOARD)
			bindUIs[(int)ControllerType.KEYBOARD].Image.sprite = God.Instance.GetKeysHandler().GetKeySprite(bind.KeyCodes[(int)ControllerType.KEYBOARD]);
		else if (ControllerType == ControllerType.GAMEPAD)
			bindUIs[(int)ControllerType.GAMEPAD].Image.sprite = God.Instance.GetKeysHandler().GetKeySprite(bind.KeyCodes[(int)ControllerType.GAMEPAD]);
	}

	public BindUI[] GetBindUIs()
	{
		return bindUIs;
	}
}
