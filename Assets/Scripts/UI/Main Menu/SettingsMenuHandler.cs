using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SettingsMenuHandler : Menu
{
	[Header("Menu")]
	[SerializeField]
	private GameObject bindingPanel;

	[Header("Sounds")]
	[SerializeField]
	public Slider soundVolumeSlider;
	[SerializeField]
	public Text soundVolumeText;

	[Header("Binding")]
	[SerializeField]
	private BindHandler[] bindHandlersArray;

	private bool isBinding = false;
	private bool hasSkippedFirstFrame = false;

	private Settings.BindType currentBind;
	private ControllerType currentControllerType;

	public override void Awake()
	{
		base.Awake();

		for (int i = 0; i < bindHandlersArray.Length; ++i)
		{
			int index = i;
			BindHandler.BindUI[] bindUIs = bindHandlersArray[i].GetBindUIs();

			bindUIs[(int)ControllerType.KEYBOARD].Button.onClick.AddListener(() => ShowRebindUI((Settings.BindType)index, ControllerType.KEYBOARD));

			if (i > 1)
				bindUIs[(int)ControllerType.GAMEPAD].Button.onClick.AddListener(() => ShowRebindUI((Settings.BindType)index, ControllerType.GAMEPAD));
		}
	}

	public override void Open(Hashtable Parameters = null)
	{
		base.Open(Parameters);
		soundVolumeSlider.Select();

		soundVolumeSlider.value = AudioListener.volume * 100f;
	}

	public void Update()
	{
		if (isBinding)
		{
			if (hasSkippedFirstFrame == false)
			{
				hasSkippedFirstFrame = true;
				return;
			}

			foreach (KeyCode vKey in System.Enum.GetValues(typeof(KeyCode)))
			{
				if (Input.GetKeyDown(vKey))
				{
					if (CheckControllerKey(vKey))
					{
						God.Instance.GetSaveHandler().SetKey(currentBind, currentControllerType, vKey);
						bindHandlersArray[(int)currentBind].UpdateKey(currentControllerType);

						HideRebindUI();

						God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.UI_VALIDATION, 0.2f);
						break;
					}
					else if (vKey == KeyCode.Escape)
					{
						HideRebindUI();
						God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.UI_SELECT, 0.2f);
					}
					else
					{
						God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.UI_ERROR, 0.2f);
					}
				}
			}
		}
	}

	public override void Back()
	{
		if (!isBinding)
		{
			God.Instance.GetSaveHandler().Save();
			God.Instance.GetMainMenuUIHandler().OpenMenu(MenuType.MAIN);
		}
	}

	/// <summary>
	/// Update volume value
	/// </summary>
	public void UpdateVolume()
	{
		soundVolumeText.text = ((int)soundVolumeSlider.value).ToString();
		AudioListener.volume = soundVolumeSlider.value / 100f;

		God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.UI_SELECT, 0.2f);
	}

	/// <summary>
	/// Show UI of binding
	/// </summary>
	/// <param name="Bind"></param>
	/// <param name="ControllerType"></param>
	public void ShowRebindUI(Settings.BindType Bind, ControllerType ControllerType)
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;

		currentBind = Bind;
		currentControllerType = ControllerType;

		bindingPanel.SetActive(true);

		EventSystem.current.sendNavigationEvents = false;

		isBinding = true;
		hasSkippedFirstFrame = false;
	}

	/// <summary>
	/// Hide rebind UI
	/// </summary>
	public void HideRebindUI()
	{
		isBinding = false;
		bindingPanel.SetActive(false);

		EventSystem.current.sendNavigationEvents = true;

		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = true;
	}

	/// <summary>
	/// Check if Key can be use as a bind
	/// </summary>
	/// <param name="Key">Key to rebind</param>
	/// <returns></returns>
	public bool CheckControllerKey(KeyCode Key)
	{
		switch(Key)
		{
			case KeyCode.A:
			case KeyCode.B:
			case KeyCode.C:
			case KeyCode.D:
			case KeyCode.E:
			case KeyCode.F:
			case KeyCode.G:
			case KeyCode.H:
			case KeyCode.I:
			case KeyCode.J:
			case KeyCode.K:
			case KeyCode.L:
			case KeyCode.M:
			case KeyCode.N:
			case KeyCode.O:
			case KeyCode.P:
			case KeyCode.Q:
			case KeyCode.R:
			case KeyCode.S:
			case KeyCode.T:
			case KeyCode.U:
			case KeyCode.V:
			case KeyCode.W:
			case KeyCode.X:
			case KeyCode.Y:
			case KeyCode.Z:
			case KeyCode.Space:
			case KeyCode.LeftArrow:
			case KeyCode.RightArrow:
			case KeyCode.UpArrow:
			case KeyCode.DownArrow:
			case KeyCode.Mouse0:
			case KeyCode.Mouse1:
				if (currentControllerType == ControllerType.KEYBOARD)
					return true;
				else
					return false;
			case KeyCode.JoystickButton0:
			case KeyCode.JoystickButton1:
			case KeyCode.JoystickButton2:
			case KeyCode.JoystickButton3:
			case KeyCode.JoystickButton4:
			case KeyCode.JoystickButton5:
				if (currentControllerType == ControllerType.GAMEPAD)
					return true;
				else
					return false;
			default:
				return false;
		}
	}
}
