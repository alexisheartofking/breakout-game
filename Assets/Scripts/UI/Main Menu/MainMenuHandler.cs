using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuHandler : Menu
{
	[Header("Menu")]

	[SerializeField]
	private CanvasGroup mainButtonsCanvas;
	[SerializeField]
	private CanvasGroup chooseControlerButtonsCanvas;

	[SerializeField]
	private Button[] mainButtonsArray;
	[SerializeField]
	private Button chooseControlerFirstButton;

	private int LastIndexToSelect = 0;

	public override void Open(Hashtable Parameters)
	{
		base.Open(Parameters);

		SelectMainButton(LastIndexToSelect);
	}

	public override void Back()
	{
		if (mainButtonsCanvas.gameObject.activeSelf == false)
			BackToMainButtons();
	}

	public void SelectMainButton(int Index)
	{
		mainButtonsArray[Index].Select();
	}

	public void GoToChoseControllerMenu()
	{
		LeanTween.alphaCanvas(mainButtonsCanvas, 0f, 0.1f).setOnComplete(OnMainButtonsAnimationDone);

		chooseControlerButtonsCanvas.gameObject.SetActive(true);
		LeanTween.alphaCanvas(chooseControlerButtonsCanvas, 1f, 0.1f);

		chooseControlerFirstButton.Select();
	}

	public void GoToGameplay(int ControllerType)
	{
		God.Instance.StartLevel(0, (ControllerType)ControllerType);
	}

	public void BackToMainButtons()
	{
		LeanTween.alphaCanvas(chooseControlerButtonsCanvas, 0f, 0.1f).setOnComplete(OnChooseControllersAnimationDone);

		mainButtonsCanvas.gameObject.SetActive(true);
		LeanTween.alphaCanvas(mainButtonsCanvas, 1f, 0.1f);

		SelectMainButton(0);
	}

	public void GoToHighscoresMenu()
	{
		LastIndexToSelect = 1;
		God.Instance.GetMainMenuUIHandler().OpenMenu(MenuType.HIGHSCORES);
	}

	public void GoToSettingsMenu()
	{
		LastIndexToSelect = 2;
		God.Instance.GetMainMenuUIHandler().OpenMenu(MenuType.SETTINGS);
	}

	public void ExitGame()
	{
		Application.Quit();
	}

	// CALLBACK

	private void OnMainButtonsAnimationDone()
	{
		mainButtonsCanvas.gameObject.SetActive(false);
	}

	private void OnChooseControllersAnimationDone()
	{
		chooseControlerButtonsCanvas.gameObject.SetActive(false);
	}
}
