using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighscoresMenuHandler : Menu
{
	[System.Serializable]
	private struct HighscoreUI
	{
		public GameObject Background;
		public Text Score;
		public Text EnemyKnockedOut;
		public Text PlayerGoals;
		public Text GoalDifference;
		public Text LevelDone;
		public Image LevelDoneImage;
	}

	[Header("Menu")]
	[SerializeField]
	private Button backButton;
	[SerializeField]
	private float timeToWaitBeforeCanGoNext = 2f;
	[SerializeField]
	private HighscoreUI[] highscoreUIArray;

	private float timerToWaitBeforeCanGoNext = 0f;
	private int highscoresCount = 5;

	public override void Open(Hashtable Parameters = null)
	{
		base.Open(Parameters);

		if (Parameters != null)
		{
			bool SetNewHighscore = (bool)Parameters["SetNewHighscore"];
			Initialize(SetNewHighscore);
		}
		else
		{
			Initialize(false);
		}

		backButton.Select();
	}

	public override void Back()
	{
		BackToMainMenu();
	}

	public void Initialize(bool InSetNewHighscore)
	{
		if (InSetNewHighscore)
			SetNewHighscore();
		else
			ShowHighscores();
	}

	private void SetNewHighscore()
	{
		timerToWaitBeforeCanGoNext = 0f;

		HighScoreSaved highScoreSaved = God.Instance.ComputeHighscore();

		for (int i = 0; i < highScoreSaved.highscoresInfos.Length; ++i)
		{
			UpdateUI(highscoreUIArray[i], highScoreSaved.highscoresInfos[i]);
			highScoreSaved.highscoresInfos[i].IsNew = false;
		}
	}

	private void ShowHighscores()
	{
		HighScoreSaved highScoreSaved = God.Instance.GetSaveHandler().GetPlayerSave().GetHighscores();

		for (int i = 0; i < highScoreSaved.highscoresInfos.Length; ++i)
			UpdateUI(highscoreUIArray[i], highScoreSaved.highscoresInfos[i]);
	}

	private void UpdateUI(HighscoreUI HighscoreUI, ScoringInfos.HighscoreInfos HighscoreInfos)
	{
		HighscoreUI.Background.SetActive(HighscoreInfos.IsNew);

		if (HighscoreInfos.IsNew)
			God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.NEW_HIGHSCORE, 0.2f);

		if (HighscoreInfos.Score < 0)
		{
			HighscoreUI.Score.text = "";
			HighscoreUI.EnemyKnockedOut.text = "";
			HighscoreUI.PlayerGoals.text = "";
			HighscoreUI.GoalDifference.text = "";
			HighscoreUI.LevelDone.text = "";
		}
		else
		{
			HighscoreUI.Score.text = HighscoreInfos.Score.ToString();
			HighscoreUI.EnemyKnockedOut.text = HighscoreInfos.EnemyKnockedOut.ToString();
			HighscoreUI.PlayerGoals.text = HighscoreInfos.PlayerGoals.ToString();
			HighscoreUI.GoalDifference.text = HighscoreInfos.GoalDifference.ToString();

			if (HighscoreInfos.LevelDone > 5)
			{
				HighscoreUI.LevelDone.text = "";
				HighscoreUI.LevelDoneImage.gameObject.SetActive(true);
			}
			else
			{
				HighscoreUI.LevelDone.text = HighscoreInfos.LevelDone.ToString();
				HighscoreUI.LevelDoneImage.gameObject.SetActive(false);
			}
		}
	}

	public void BackToMainMenu()
	{
		God.Instance.GetMainMenuUIHandler().OpenMenu(MenuType.MAIN);
	}
}
