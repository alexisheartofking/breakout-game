using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CanvasGroup))]
public class Menu : MonoBehaviour
{
	[Header("Open Animation")]
	[SerializeField]
	protected float openAnimationTime = 0.1f;

	[Header("Close Animation")]
	[SerializeField]
	protected float closeAnimationTime = 0.1f;

	private CanvasGroup canvasGroup;

	virtual public void Awake()
	{
		canvasGroup = GetComponent<CanvasGroup>();
	}

	virtual public void Open(Hashtable Parameters = null)
	{
		gameObject.SetActive(true);
		LeanTween.alphaCanvas(canvasGroup, 1f, openAnimationTime).setFrom(0f);
	}

	virtual public void Close()
	{
		LeanTween.alphaCanvas(canvasGroup, 0f, closeAnimationTime).setFrom(1f).setOnComplete(() => { gameObject.SetActive(false); });
	}

	virtual public void Back()
	{
	}
}
