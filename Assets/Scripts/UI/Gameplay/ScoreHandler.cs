using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreHandler : MonoBehaviour
{
	[SerializeField]
	private Text team1Text;
	[SerializeField]
	private Text team2Text;

	private int team1Score;
	private int team2Score;

	private TeamHandler.TeamInfos team1Infos;
	private TeamHandler.TeamInfos team2Infos;

	/// <summary>
	/// Set scoreboard
	/// </summary>
	/// <param name="Team1Name">First team</param>
	/// <param name="Team1Score">First team score</param>
	/// <param name="Team2Name">Second team</param>
	/// <param name="Team2Score">Second team score</param>
	public void SetScoreboard(Team Team1Name, int Team1Score, Team Team2Name, int Team2Score)
	{
		team1Score = Team1Score;
		team2Score = Team2Score;

		SetTeamNameOnUI(Team1Name, ref team1Infos, team1Text);
		SetTeamNameOnUI(Team2Name, ref team2Infos, team2Text);

		UpdateScoreUI();
	}

	/// <summary>
	/// Set name (3 letters) on UI from TeamName
	/// </summary>
	/// <param name="TeamName">Team enum to show</param>
	/// <param name="TeamText">UI Text to set team name</param>
	private void SetTeamNameOnUI(Team TeamName, ref TeamHandler.TeamInfos TeamInfos, Text TeamText)
	{
		TeamInfos = God.Instance.GetTeamHandler().GetTeamInfos(TeamName);
		TeamText.color = TeamInfos.Color;
	}

	/// <summary>
	/// Update score in scorboard UI
	/// </summary>
	private void UpdateScoreUI()
	{
		team1Text.text = team1Infos.ShortName +  " " + team1Score;
		team2Text.text = team2Score +  " " + team2Infos.ShortName;
	}

	/// <summary>
	/// Add a goal to first team
	/// </summary>
	public void FirstTeamScored()
	{
		++team1Score;
		UpdateScoreUI();
	}

	/// <summary>
	/// Add a goal to second team
	/// </summary>
	public void SecondTeamScored()
	{
		++team2Score;
		UpdateScoreUI();
	}

	/// <summary>
	/// Get if player has won the match
	/// </summary>
	/// <returns>Has won or not</returns>
	public bool HasPlayerWon()
	{
		if (team1Score > team2Score)
			return true;

		return false;
	}

	/// <summary>
	/// Compute goal difference and add it to score
	/// </summary>
	public void ComputeScoreDifferenceToScore()
	{
		God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetScoringHandler().AddScore(ScoringInfos.ScoringType.GOAL_DIFFERENCE, team1Score - team2Score);
	}

	public TeamHandler.TeamInfos GetTeam1Infos()
	{
		return team1Infos;
	}

	public TeamHandler.TeamInfos GetTeam2Infos()
	{
		return team2Infos;
	}
}
