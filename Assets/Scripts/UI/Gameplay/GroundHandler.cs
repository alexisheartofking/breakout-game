using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GroundState
{
	HALF,
	FULL
}

public class GroundHandler : MonoBehaviour
{
	[SerializeField]
	private GameObject halfGround;
	[SerializeField]
	private GameObject fullGround;

	public void SetGroundState(GroundState GroundState)
	{
		if (GroundState == GroundState.FULL)
		{
			halfGround.SetActive(false);
			fullGround.SetActive(true);
		}
		else if (GroundState == GroundState.HALF)
		{
			halfGround.SetActive(true);
			fullGround.SetActive(false);
		}
	}
}
