using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlertHandler : MonoBehaviour
{
	[SerializeField]
	private Text alertText;
	[SerializeField]
	private Image alertBackground;

	/// <summary>
	/// Start alert animation
	/// </summary>
	/// <param name="alertString">String to show in alert</param>
	/// <param name="TimeAnimation">Animation duration</param>
	public void StartAlert(string AlertString, float TimeAnimation, Color AlertColor)
	{
		AlertColor.a = 200f / 255f;

		alertText.text = AlertString;
		alertText.gameObject.SetActive(true);

		alertBackground.color = AlertColor;
		gameObject.SetActive(true);

		StartCoroutine(AlertAnimation(TimeAnimation));
	}

	/// <summary>
	/// Start blink alert animation
	/// </summary>
	/// <param name="alertString">String to show in alert</param>
	public void StartBlinkAlert(string AlertString, float TimeAnimation, Color AlertColor)
	{
		AlertColor.a = 200f / 255f;

		alertText.text = AlertString;
		alertText.gameObject.SetActive(true);

		alertBackground.color = AlertColor;
		gameObject.SetActive(true);

		StartCoroutine(AlertBlinkAnimation(TimeAnimation));
	}

	public void StartCongratulationsAlert(string AlertString, float TimeAnimation)
	{
		alertBackground.color = new Color(210f / 255f, 204f / 255f, 24f / 255f, 0.5f);
		alertText.text = AlertString;

		gameObject.SetActive(true);

		StartCoroutine(AlertBlinkAnimation(TimeAnimation));
	}

	private IEnumerator AlertAnimation(float TimeAnimation)
	{
		yield return new WaitForSeconds(TimeAnimation);

		gameObject.SetActive(false);

		God.Instance.GetGameplayHandler().RestartGameplay();
	}

	private IEnumerator AlertBlinkAnimation(float TimeAnimation)
	{
		yield return new WaitForSeconds(0.5f);

		for (int i = 0; i < (int)TimeAnimation - 1; i++)
		{
			alertText.gameObject.SetActive(false);
			yield return new WaitForSeconds(0.5f);
			alertText.gameObject.SetActive(true);
			yield return new WaitForSeconds(0.5f);
		}

		gameObject.SetActive(false);

		God.Instance.GetGameplayHandler().RestartGameplay();
	}
}
