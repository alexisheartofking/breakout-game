using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuHandler : MonoBehaviour
{
	[SerializeField]
	private Button firstButton;

	private bool isOpen = false;

	/// <summary>
	/// Open pause menu
	/// </summary>
	/// <returns>Return if need to skip frame for input doublon</returns>
	public bool Open()
	{
		if (isOpen)
		{
			Close();
			return true;
		}

		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = true;

		Time.timeScale = 0f;

		gameObject.SetActive(true);
		isOpen = true;

		firstButton.Select();
		return false;
	}

	/// <summary>
	/// Close pause menu
	/// </summary>
	public void Close()
	{
		Time.timeScale = 1f;

		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;

		gameObject.SetActive(false);
		isOpen = false;

		God.Instance.GetGameplayHandler().SkipNextFrame();
	}

	/// <summary>
	/// Go back to main menu
	/// </summary>
	public void GoToMainMenu()
	{
		Time.timeScale = 1f;

		God.Instance.GoBackToMainMenu();
	}

	/// <summary>
	/// Exit game
	/// </summary>
	public void ExitGame()
	{
		Application.Quit();
	}
}
