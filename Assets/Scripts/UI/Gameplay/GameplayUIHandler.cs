using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayUIHandler : MonoBehaviour
{
	[SerializeField]
	private ScoreHandler scoreHandler;
	[SerializeField]
	private ScoringHandler scoringHandler;
	[SerializeField]
	private TimerHandler timerHandler;
	[SerializeField]
	private AlertHandler alertHandler;
	[SerializeField]
	private SpecialShotUIHandler specialShotUIHandler;
	[SerializeField]
	private LevelExplanationUIHandler levelExplanationUIHandler;
	[SerializeField]
	private ButtonTutorialHandler buttonTutorialHandler;
	[SerializeField]
	private PauseMenuHandler pauseMenuHandler;

	public ScoreHandler GetScoreHandler()
	{
		if (scoreHandler == null)
			Debug.LogError("scoreHandler is null");

		return scoreHandler;
	}

	public ScoringHandler GetScoringHandler()
	{
		if (scoringHandler == null)
			Debug.LogError("scoringHandler is null");

		return scoringHandler;
	}

	public TimerHandler GetTimerHandler()
	{
		if (timerHandler == null)
			Debug.LogError("timerHandler is null");

		return timerHandler;
	}

	public AlertHandler GetAlertHandler()
	{
		if (alertHandler == null)
			Debug.LogError("alertHandler is null");

		return alertHandler;
	}

	public SpecialShotUIHandler GetSpecialShotUIHandler()
	{
		if (specialShotUIHandler == null)
			Debug.LogError("specialShotUIHandler is null");

		return specialShotUIHandler;
	}

	public LevelExplanationUIHandler GetLevelExplanationUIHandler()
	{
		if (levelExplanationUIHandler == null)
			Debug.LogError("levelExplanationUIHandler is null");

		return levelExplanationUIHandler;
	}

	public ButtonTutorialHandler GetButtonTutorialHandler()
	{
		if (buttonTutorialHandler == null)
			Debug.LogError("buttonTutorialHandler is null");

		return buttonTutorialHandler;
	}

	public PauseMenuHandler GetPauseMenuHandler()
	{
		if (pauseMenuHandler == null)
			Debug.LogError("pauseMenuHandler is null");

		return pauseMenuHandler;
	}
}
