using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialShotUIHandler : MonoBehaviour
{
	[System.Serializable]
	private struct UIInfos
	{
		public Image Image;
		public KeyHandler KeyHandler;
		public CanvasGroup CanvasGroup;
	}

	[SerializeField]
	private UIInfos[] uIInfos;

	/// <summary>
	/// Set visibility of the special shot UI
	/// </summary>
	/// <param name="Show"></param>
	public void SetState(bool Show)
	{
		gameObject.SetActive(Show);
	}

	/// <summary>
	/// Update special shot icon color
	/// </summary>
	/// <param name="SpecialShot1Color">Special shot 1 color</param>
	/// <param name="SpecialShot2Color">Special shot 2 color</param>
	/// <param name="SpecialShot3Color">Special shot 3 color</param>
	public void UpdateSpecialShotColor(Color SpecialShot1Color, Color SpecialShot2Color, Color SpecialShot3Color)
	{
		uIInfos[0].Image.color = SpecialShot1Color;
		uIInfos[1].Image.color = SpecialShot2Color;
		uIInfos[2].Image.color = SpecialShot3Color;
	}

	/// <summary>
	/// Update UI to show special shots available
	/// </summary>
	/// <param name="CanUse">Can use special shot</param>
	public void CanUseSpecialShot(bool CanUse)
	{
		for (int i = 0; i < uIInfos.Length; ++i)
		{
			if (CanUse)
			{
				uIInfos[i].CanvasGroup.alpha = 1f;
				God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.SPECIAL_READY, 0.03f);
			}
			else
				uIInfos[i].CanvasGroup.alpha = 0.1f;
		}
	}

	/// <summary>
	/// Update UI with special shot used
	/// </summary>
	/// <param name="State">Special shot state</param>
	public void UseSpecialShot(BallBehaviour.State State)
	{
		for (int i = 0; i < uIInfos.Length; ++i)
		{
			if (i == (int)State)
				uIInfos[i].CanvasGroup.alpha = 1f;
			else
				uIInfos[i].CanvasGroup.alpha = 0.1f;
		}
	}

	/// <summary>
	/// Set special shot key on UI
	/// </summary>
	/// <param name="SpecialShot1Key">Special 1 shot key (eagle)</param>
	/// <param name="SpecialShot2Key">Special 2 shot key (curve)</param>
	/// <param name="SpecialShot3Key">Special 3 shot key (triple)</param>
	public void SetSpecialShotsKeys(KeyCode SpecialShot1Key, KeyCode SpecialShot2Key, KeyCode SpecialShot3Key)
	{
		uIInfos[0].KeyHandler.SetKey(SpecialShot1Key);
		uIInfos[1].KeyHandler.SetKey(SpecialShot2Key);
		uIInfos[2].KeyHandler.SetKey(SpecialShot3Key);
	}
}
