using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelExplanationUIHandler : MonoBehaviour
{
	[SerializeField]
	private Image special1Image;
	[SerializeField]
	private Image special2Image;
	[SerializeField]
	private Image special3Image;

	[SerializeField]
	private GameObject[] levelExplainations;

	private int currentLevelIndex;

	/// <summary>
	/// Update special shot icon color
	/// </summary>
	/// <param name="SpecialShot1Color"></param>
	/// <param name="SpecialShot2Color"></param>
	/// <param name="SpecialShot3Color"></param>
	public void UpdateSpecialShotColor(Color SpecialShot1Color, Color SpecialShot2Color, Color SpecialShot3Color)
	{
		special1Image.color = SpecialShot1Color;
		special2Image.color = SpecialShot2Color;
		special3Image.color = SpecialShot3Color;
	}

	/// <summary>
	/// Show level explanation
	/// </summary>
	/// <param name="CurrentLevelIndex"></param>
	public void ShowLevelExplanation(int CurrentLevelIndex)
	{
		currentLevelIndex = CurrentLevelIndex;

		gameObject.SetActive(true);
		levelExplainations[currentLevelIndex].SetActive(true);
	}

	/// <summary>
	/// Hide level explaination
	/// </summary>
	public void HideLevelExplanation()
	{
		gameObject.SetActive(false);
		levelExplainations[currentLevelIndex].SetActive(false);
	}
}
