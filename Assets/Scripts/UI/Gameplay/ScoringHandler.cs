using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoringHandler : MonoBehaviour
{
	[SerializeField]
	private Text scoringText;

	private int currentScore = 0;

	/// <summary>
	/// Add score
	/// </summary>
	/// <param name="Coefficient">Score to add</param>
	public void AddScore(ScoringInfos.ScoringType ScoringType, int Coefficient)
	{
		currentScore += God.Instance.GetScoringInfos().GetScoringInfoAndUpdateStats(ScoringType, Coefficient) * Coefficient;
		UpdateUI();
	}

	/// <summary>
	/// Update score UI
	/// </summary>
	private void UpdateUI()
	{
		scoringText.text = Mathf.Max(0, currentScore).ToString("0000000");
	}

	public int GetScore()
	{
		return currentScore;
	}
}
