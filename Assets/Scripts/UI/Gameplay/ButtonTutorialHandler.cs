using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonTutorialHandler : MonoBehaviour
{
	[SerializeField]
	private GameObject[] tutorialButtons;
	[SerializeField]
	private Image moveLeftKeyboardImage;
	[SerializeField]
	private Image moveRightKeyboardImage;
	[SerializeField]
	private Image tacleKeyboardImage;
	[SerializeField]
	private Image tacleGamepadImage;

	private ControllerType lastControllerTutorialShown;

	private void Awake()
	{
		PlayerController.PlayerKeys playerKeys = God.Instance.GetPlayerKeys();

		moveLeftKeyboardImage.sprite = God.Instance.GetKeysHandler().GetKeySprite(playerKeys.LeftKey);
		moveRightKeyboardImage.sprite = God.Instance.GetKeysHandler().GetKeySprite(playerKeys.RightKey);
		tacleKeyboardImage.sprite = God.Instance.GetKeysHandler().GetKeySprite(playerKeys.TacleKey);
		tacleGamepadImage.sprite = God.Instance.GetKeysHandler().GetKeySprite(playerKeys.TacleKey);
	}

	/// <summary>
	/// Show tutorial button
	/// </summary>
	/// <param name="ControllerType">Controller type tutorial to show</param>
	public void ShowButtonTutorial(ControllerType ControllerType)
	{
		lastControllerTutorialShown = ControllerType;

		gameObject.SetActive(true);
		tutorialButtons[(int)ControllerType].SetActive(true);
	}

	/// <summary>
	/// Hide tutorial button
	/// </summary>
	public void HideTutorial()
	{
		gameObject.SetActive(false);
		tutorialButtons[(int)lastControllerTutorialShown].SetActive(false);
	}
}
