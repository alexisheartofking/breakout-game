using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerHandler : MonoBehaviour
{
	public enum HalfState
	{
		FIRST_HALF = 0,
		SECOND_HALF = 1
	}

	// Delegate
	public delegate void HalfTimeOver(HalfState HalfState);
	public HalfTimeOver d_HalfTimeOver;

	[SerializeField]
	private Text scoreText;
	[SerializeField]
	private float timeMultiplier = 60;
	[SerializeField]
	private float halfTime = 45;

	private HalfState halfState = HalfState.FIRST_HALF;
	private float currentTime;
	private bool isPaused;

	private void Awake()
	{
		Reset(HalfState.FIRST_HALF);
		//StartTimer();
	}

	private void Update()
	{
		if (isPaused)
			return;

		currentTime += Time.deltaTime * timeMultiplier;

		if (halfState == HalfState.FIRST_HALF)
		{
			if (currentTime >= halfTime * 60f)
			{
				currentTime = halfTime * 60f;
				isPaused = true;

				if (d_HalfTimeOver != null)
					d_HalfTimeOver(HalfState.FIRST_HALF);

				halfState = HalfState.SECOND_HALF;
			}
		}
		else if (halfState == HalfState.SECOND_HALF)
		{
			if (currentTime >= halfTime * 2f * 60f)
			{
				currentTime = halfTime * 2f * 60f;
				isPaused = true;

				if (d_HalfTimeOver != null)
					d_HalfTimeOver(HalfState.SECOND_HALF);
			}
		}

		UpdateUI();
	}

	/// <summary>
	/// Start timer
	/// </summary>
	public void StartTimer()
	{
		isPaused = false;
	}

	/// <summary>
	/// Set timer to pause
	/// </summary>
	public void PauseTimer()
	{
		isPaused = true;
	}

	/// <summary>
	/// Reset to first or second half
	/// </summary>
	/// <param name="HalfState">Half to reset</param>
	public void Reset(HalfState HalfState)
	{
		halfState = HalfState;
		currentTime = (int)halfState * halfTime * 60f;
		isPaused = true;

		UpdateUI();
	}

	/// <summary>
	/// Update timer UI
	/// </summary>
	private void UpdateUI()
	{
		float minutes = Mathf.Floor(currentTime / 60f);
		float secondes = Mathf.Floor(currentTime - (minutes * 60f));

		scoreText.text = minutes.ToString("00") + ":" + secondes.ToString("00");
	}
}
