using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class FakeBall : MonoBehaviour
{
	[HideInInspector]
	public int Damage;

	private Vector2 bounceOnPlayerRatioRange;
	private Rigidbody2D rb2d;
	private float speed;
	private float tripleShotTime = 99f;

	private PlayerController playerController;

	private float currentTime = 0f;

	private void Awake()
	{
		rb2d = GetComponent<Rigidbody2D>();
	}

	public void Initialize(int InDamage, float InSpeed, Vector2 InBounceOnPlayerRatioRange, Vector2 Velocity, float InTripleShotTime)
	{
		playerController = God.Instance.GetGameplayHandler().GetPlayerController();
		Damage = InDamage;
		speed = InSpeed;
		bounceOnPlayerRatioRange = InBounceOnPlayerRatioRange;
		rb2d.velocity = Velocity;
		tripleShotTime = InTripleShotTime;
	}

	private void Update()
	{
		// Keep ball velocity
		if (rb2d.velocity.normalized == Vector2.zero)
		{
			rb2d.velocity = speed * transform.up;
		}
		else
		{
			//Protect from an horizontal velocity that can make waste a lot of time
			Vector2 veloNorm = rb2d.velocity.normalized;

			if (veloNorm.y < 0.3f && veloNorm.y > -0.3f)
			{
				float value = 0f;
				if (veloNorm.y - 0.3f < -0.3f)
					value = -0.3f;
				else
					value = 0.3f;

				rb2d.velocity = speed * new Vector2(veloNorm.x - value, veloNorm.x + (value - veloNorm.x));
			}
			else
				rb2d.velocity = speed * veloNorm.normalized;
		}

		currentTime += Time.deltaTime;

		if (currentTime >= tripleShotTime)
			Destroy(gameObject);
	}

	private void OnCollisionEnter2D(Collision2D Collision)
	{
		if (Collision.collider.CompareTag("Player"))
		{
			float bounceOnPlayerRatio = Random.Range(bounceOnPlayerRatioRange.x, bounceOnPlayerRatioRange.y);

			// Change velocity upon player direction
			if (playerController.direction.x < 0)
				rb2d.velocity = speed * new Vector2(-rb2d.velocity.y * bounceOnPlayerRatio, rb2d.velocity.y).normalized;
			else if (playerController.direction.x > 0)
				rb2d.velocity = speed * new Vector2(rb2d.velocity.y * bounceOnPlayerRatio, rb2d.velocity.y).normalized;
		}
		else if (Collision.collider.CompareTag("OwnGoal"))
		{
			God.Instance.GetGameplayHandler().EnemyScored();
		}
		else if (Collision.collider.CompareTag("EnemyGoal"))
		{
			God.Instance.GetGameplayHandler().PlayerScored();
		}
		else if (Collision.collider.CompareTag("Enemy"))
		{
			EnemyHittable enemyHittable = Collision.transform.GetComponent<EnemyHittable>();
			enemyHittable.Hit(-Damage);
		}
	}
}
