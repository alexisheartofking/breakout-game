using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
	[SerializeField]
	private Transform player;
	[SerializeField]
	private Vector3 offset;

    void LateUpdate()
    {
		Vector3 position = transform.position;
		position = player.transform.position + offset;
		transform.position = position;
    }

	/// <summary>
	/// Replace ball in front of player
	/// </summary>
	public void ReplaceBall()
	{
		LateUpdate();
	}
}
