using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(FollowPlayer))]
[RequireComponent(typeof(BallArrow))]
public class BallBehaviour : MonoBehaviour
{
	public enum State
	{
		EAGLE = 0,
		CURVE = 1,
		TRIPLE = 2,
		STOP = 3,
		NORMAL = 4,
		SPECIAL = 5
	}

	[HideInInspector]
	public State CurrentState = State.NORMAL;

	[SerializeField]
	private SpriteRenderer ballSprite;
	[SerializeField]
	private GameObject ballShadow;
	[SerializeField]
	private GameObject fakeBallPrefab;

	[Header("Ball Settings")]
	[SerializeField]
	private float baseSpeed = 8f;
	[SerializeField]
	private float maxSpeed = 15f;
	[SerializeField]
	private float speedGainPerHit = 1f;
	[SerializeField]
	private Vector2 bounceOnPlayerRatioRange;

	// Shoot State
	[Header("Normal Shoot")]
	public int normalShootDamage = 1;

	[Header("Special Mode")]
	public float specialModeDamage = 2f;

	[Header("Eagle Shoot")]
	public Color EagleShotColor;
	[Range(0, 100)]
	public float EagleShotGoalSuccess;

	[Header("Curve Shoot")]
	public Color CurveShotColor;
	public float curveShotDamage = 50f;
	public float curveShotSpeed = 20f;
	public float curveShotForce = 3f;

	[Header("Triple Shoot")]
	public Color tripleShotColor;
	[SerializeField]
	private float tripleShotTime = 5f;
	private float baseTripleShotSpeed = 5f;

	private Rigidbody2D rb2d;
	private Collider2D coll2d;
	private FollowPlayer followPlayer;
	private BallArrow ballArrow;
	private PlayerController playerController;

	private bool isShoot = false;
	private float currentSpeed = 2f;
	private Vector3 direction = Vector3.zero;

	private List<FakeBall> fakeBalls;
	private float currentTimeTripleShot;

	private Transform lastCollider;

	private bool hasShoot = false;
	private bool keepLastVelocity = false;

	private Vector2 lastVelocity;

	public void Awake()
	{
		rb2d = GetComponent<Rigidbody2D>();
		followPlayer = GetComponent<FollowPlayer>();
		ballArrow = GetComponent<BallArrow>();
		coll2d = GetComponent<Collider2D>();

		fakeBalls = new List<FakeBall>();

	}

	public void Initialize()
	{
		playerController = God.Instance.GetGameplayHandler().GetPlayerController();

		ballArrow.Initialize();

		followPlayer.enabled = true;
		hasShoot = false;
		keepLastVelocity = false;
		
		currentSpeed = baseSpeed;

		SetState(State.STOP);

		God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetSpecialShotUIHandler().UpdateSpecialShotColor(EagleShotColor, CurveShotColor, tripleShotColor);
		God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetLevelExplanationUIHandler().UpdateSpecialShotColor(EagleShotColor, CurveShotColor, tripleShotColor);
	}

	public void FixedUpdate()
	{
		if (hasShoot == false)
			return;

		if (CurrentState == State.TRIPLE)
		{
			currentTimeTripleShot += Time.fixedDeltaTime;

			if (currentTimeTripleShot > tripleShotTime)
			{
				ResetBallState();
			}
		}
		else if (CurrentState == State.CURVE)
		{
			lastVelocity = rb2d.velocity;

			rb2d.AddForce(Vector2.up * curveShotForce);
			rb2d.velocity = currentSpeed * rb2d.velocity.normalized;
		}

		if (keepLastVelocity)
		{
			rb2d.velocity = lastVelocity;
		}

		// Keep ball velocity
		if (rb2d.velocity.normalized == Vector2.zero)
		{
			rb2d.velocity = currentSpeed * transform.up;
		}
		else if (CurrentState != State.CURVE)
		{
			//Protect from an horizontal velocity that can make waste a lot of time
			Vector2 veloNorm = rb2d.velocity.normalized;

			if (veloNorm.y < 0.3f && veloNorm.y > -0.3f)
			{
				float value = 0f;
				if (veloNorm.y - 0.3f < -0.3f)
					value = -0.3f;
				else
					value = 0.3f;

				rb2d.velocity = currentSpeed * new Vector2(veloNorm.x - value, veloNorm.x + (value - veloNorm.x));
			}
			else
				rb2d.velocity = currentSpeed * veloNorm.normalized;
		}
	}

	private void OnCollisionEnter2D(Collision2D Collision)
	{
		if (keepLastVelocity)
			keepLastVelocity = false;

		if (Collision.collider.CompareTag("Player"))
		{
			float bounceOnPlayerRatio = Random.Range(bounceOnPlayerRatioRange.x, bounceOnPlayerRatioRange.y);

			// Change velocity upon player direction
			if (playerController.direction.x < 0)
				rb2d.velocity = currentSpeed * new Vector2(-rb2d.velocity.y * bounceOnPlayerRatio, rb2d.velocity.y).normalized;
			else if (playerController.direction.x > 0)
				rb2d.velocity = currentSpeed * new Vector2(rb2d.velocity.y * bounceOnPlayerRatio, rb2d.velocity.y).normalized;
		}
		else if (Collision.collider.CompareTag("OwnGoal"))
		{
			God.Instance.GetGameplayHandler().EnemyScored();
		}
		else if (Collision.collider.CompareTag("EnemyGoal"))
		{
			if (gameObject.layer == 7)
			{
				float rng = Random.Range(0f, 100f);

				if (rng <= EagleShotGoalSuccess)
					God.Instance.GetGameplayHandler().PlayerScored();
				else
					God.Instance.GetGameplayHandler().BallOutOfBounds();
			}
			else
			{
				God.Instance.GetGameplayHandler().PlayerScored();
			}
		}
		else if (Collision.collider.CompareTag("Enemy"))
		{
			EnemyHittable enemyHittable = Collision.transform.GetComponent<EnemyHittable>();

			switch (CurrentState)
			{
				case BallBehaviour.State.NORMAL:
				case BallBehaviour.State.SPECIAL:
					enemyHittable.Hit(-specialModeDamage);
					break;
				case BallBehaviour.State.CURVE:
					if (enemyHittable.Hit(-curveShotDamage))
					{
						keepLastVelocity = true;
						rb2d.velocity = lastVelocity;
					}
					break;
			}

		}
		else if (CurrentState == State.EAGLE)
		{
			God.Instance.GetGameplayHandler().BallOutOfBounds();
		}

		if (CurrentState == State.CURVE)
			ResetBallState();

		// If hit Character Layer
		if (hasShoot && CurrentState == State.NORMAL && (Collision.gameObject.layer == 6 || Collision.gameObject.layer == 9))
		{
			currentSpeed = currentSpeed + speedGainPerHit;

			// When currentSpeed is maxSpeed then ball is ready for special shot
			if (currentSpeed >= maxSpeed)
			{
				currentSpeed = maxSpeed;
				SetState(State.SPECIAL);
			}
		}

		God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.BALL_IMPACT, 0.25f);
	}

	/// <summary>
	/// Stop the ball
	/// </summary>
	public void Stop()
	{
		hasShoot = false;
		rb2d.velocity = Vector3.zero;
		coll2d.enabled = false;
	}

	/// <summary>
	/// Replace ball to player's foot
	/// </summary>
	public void Reset()
	{
		ballArrow.Initialize();
		followPlayer.enabled = true;

		rb2d.velocity = Vector2.zero;
		currentSpeed = baseSpeed;
		isShoot = false;

		SetState(State.STOP);
	}

	/// <summary>
	/// Shoot the ball from the player
	/// </summary>
	public void Shoot(State NewState)
	{
		ballArrow.Stop();
		followPlayer.enabled = false;
		hasShoot = true;

		God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.BALL_IMPACT, 0.25f);

		SetState(NewState);
	}

	/// <summary>
	/// Reset ball state to NORMAL
	/// </summary>
	public void ResetBallState()
	{
		CurrentState = State.NORMAL;
		coll2d.enabled = true;
		gameObject.layer = 0;
		currentSpeed = baseSpeed;

		ballShadow.SetActive(false);
		ballSprite.color = Color.white;

		God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetSpecialShotUIHandler().CanUseSpecialShot(false);
	}

	/// <summary>
	/// Set ball state
	/// </summary>
	/// <param name="NewState">New state to set</param>
	public void SetState(State NewState)
	{
		CurrentState = NewState;

		switch (CurrentState)
		{
			case State.STOP:
				coll2d.enabled = false;
				gameObject.layer = 8;

				ballSprite.color = Color.white;
				ballShadow.SetActive(false);

				ClearFakeBalls();

				God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetSpecialShotUIHandler().CanUseSpecialShot(false);
				break;
			case State.NORMAL:
				coll2d.enabled = true;
				gameObject.layer = 0;
				currentSpeed = baseSpeed;

				rb2d.velocity = currentSpeed * ballArrow.arrowPivot.transform.up;
				ballShadow.SetActive(false);
				ballSprite.color = Color.white;

				ClearFakeBalls();

				God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetSpecialShotUIHandler().CanUseSpecialShot(false);
				break;
			case State.SPECIAL:
				ballSprite.color = Color.red;
				God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetSpecialShotUIHandler().CanUseSpecialShot(true);
				break;
			case State.EAGLE:
				EagleShot();
				God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.BALL_SPECIAL_SHOT, 0.5f);
				break;
			case State.CURVE:
				CurveShot();
				God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.BALL_SPECIAL_SHOT, 0.5f);
				break;
			case State.TRIPLE:
				TripleShot();
				God.Instance.GetSoundsHandler().PlaySound(SoundsHandler.SoundType.BALL_SPECIAL_SHOT, 0.5f);
				break;
		}
	}

	/// <summary>
	/// Handle eagle shot
	/// </summary>
	private void EagleShot()
	{
		followPlayer.ReplaceBall();

		currentSpeed = baseSpeed;
		rb2d.velocity = currentSpeed * Vector3.up;

		gameObject.layer = 7;
		ballShadow.SetActive(true);

		ballSprite.color = EagleShotColor;

		God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetSpecialShotUIHandler().UseSpecialShot(CurrentState);
	}

	/// <summary>
	/// Handle curve shot
	/// </summary>
	private void CurveShot()
	{
		followPlayer.ReplaceBall();
		currentSpeed = curveShotSpeed;
		ballSprite.color = CurveShotColor;
		God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetSpecialShotUIHandler().UseSpecialShot(CurrentState);

		if (God.Instance.GetGameplayHandler().GetPlayerController().transform.position.x > 0)
			rb2d.velocity = new Vector2(-1f * currentSpeed, 0f);
		else
			rb2d.velocity = new Vector2(1f * currentSpeed, 0f);
	}

	/// <summary>
	/// Handle triple shot
	/// </summary>
	private void TripleShot()
	{
		followPlayer.ReplaceBall();
		currentSpeed = baseTripleShotSpeed;
		rb2d.velocity = currentSpeed * Vector3.up;

		ballSprite.color = tripleShotColor;
		currentTimeTripleShot = 0f;

		God.Instance.GetGameplayHandler().GetGameplayUIHandler().GetSpecialShotUIHandler().UseSpecialShot(CurrentState);

		CreateFakeBall(baseTripleShotSpeed - 1, new Vector2(0.5f, 0.5f));
		CreateFakeBall(baseTripleShotSpeed - 2, new Vector2(-0.5f, 0.5f));
	}

	/// <summary>
	/// Clear all fake balls
	/// </summary>
	private void ClearFakeBalls()
	{
		if (fakeBalls != null)
		{
			for (int i = 0; i < fakeBalls.Count; ++i)
			{
				FakeBall fakeBall = fakeBalls[i];

				if (fakeBall != null)
					Destroy(fakeBall.gameObject);
			}

			fakeBalls.Clear();
		}
	}

	/// <summary>
	/// Create and initialize a Fake ball
	/// </summary>
	/// <param name="Direction">Set left or right direction (-1 or 1)</param>
	private void CreateFakeBall(float Speed, Vector2 Direction)
	{
		FakeBall fakeBall = GameObject.Instantiate(fakeBallPrefab, transform.position, Quaternion.identity).GetComponent<FakeBall>();
		fakeBalls.Add(fakeBall);

		fakeBall.Initialize(normalShootDamage, Speed, bounceOnPlayerRatioRange, Direction, tripleShotTime);
	}
}
