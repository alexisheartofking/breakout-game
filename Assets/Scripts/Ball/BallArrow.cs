using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallArrow : MonoBehaviour
{
	public Transform arrowPivot;

	[SerializeField]
	private Vector2 angleRange = new Vector2(-8f, 80f);
	[SerializeField]
	private float speed = 3f;

	private float angleDirection = 1f;
	private float currentAngle = 0f;

	public void Initialize()
	{
		enabled = true;
		arrowPivot.gameObject.SetActive(true);
		arrowPivot.rotation = Quaternion.Euler(0f, 0f, 0f);
		angleDirection = 1f;
	}

	private void Update()
	{
		//Arrow movements
		currentAngle = arrowPivot.localRotation.eulerAngles.z;
		currentAngle += speed * Time.deltaTime * angleDirection;

		// Arrow change direction
		if (currentAngle >= angleRange.y)
		{
			angleDirection = -1f;
			currentAngle = angleRange.y;
		}
		else if (currentAngle <= angleRange.x)
		{
			angleDirection = 1f;
			currentAngle = angleRange.x;
		}

		arrowPivot.localRotation = Quaternion.Euler(0f, 0f, currentAngle);
	}

	/// <summary>
	/// Stop the arrow movement and hide it
	/// </summary>
	public void Stop()
	{
		enabled = false;
		arrowPivot.gameObject.SetActive(false);
	}
}
